//
//  WXSdkManager.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit

typealias WXComplite = ((_ result:[String:Any]?, _ message:String)->Void)

class WXSdkManager: NSObject, WXApiDelegate {
    
    static let share = WXSdkManager()
    var wxComplite:WXComplite?
    //MARK: ===================== 微信登录
    @objc func login(complite:@escaping WXComplite) {
        wxComplite = complite
        if !WXApi.isWXAppInstalled() {
            MBProgressHUD.ex_showPlainText("请安装微信APP!", view: nil) 
            wxComplite?(nil,"请安装微信APP!")
            return
        }
        
        //构造SendAuthReq结构体
        let req = SendAuthReq()
        req.openID = kWXAPPID
        req.scope = "snsapi_userinfo"
        req.state = "123"//用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止 csrf 攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加 session 进行校验
        
        //第三方向微信终端发送一个SendAuthReq消息结构
        WXApi.send(req)
    }
    
    func sendText(text:String, inScene: WXScene, complite:@escaping WXComplite) {
        wxComplite = complite
        let req = SendMessageToWXReq()
        req.text = text
        req.bText = true
        req.scene = Int32(inScene.rawValue)
        WXApi.send(req)
    }
    
    /// 微信sdk分享链接
    /// - Parameters:
    ///   - urlString: 链接
    ///   - tagName: 分享标签
    ///   - title: 分享标题
    ///   - description: 分享描述
    ///   - thumbImage: 分享图片
    ///   - scene: 分享目标,会话(WXSceneSession)或者朋友圈(WXSceneTimeline)
    func sendLinkURL(_ urlString: String, tagName: String?, title: String, description: String?, thumbImage: UIImage, in scene: WXScene, complite:@escaping WXComplite){
        wxComplite = complite
        
        let ext = WXWebpageObject()
        ext.webpageUrl = urlString
        
        let message = WXMediaMessage()
        message.title = title
        message.description = description ?? ""
        message.mediaObject = ext
        message.messageExt = nil
        message.messageAction = nil
        message.setThumbImage(thumbImage)
        message.mediaTagName = tagName
        
        let req = SendMessageToWXReq()
        req.bText = false
        req.scene = Int32(scene.rawValue)
        req.message = message
        WXApi.send(req)
    }
    
    //MARK: ======================== WXApiDelegate
    func onReq(_ req: BaseReq) {
        print("aaaaaaaaaaaaaa")
    }
    
    func onResp(_ resp: BaseResp) {
        if resp.isKind(of: SendAuthResp.self) {
            if resp.errCode == 0 {
                let _resp = resp as! SendAuthResp
                if let code = _resp.code {
                    self.wxComplite?(["code":code], "")
                } else {
                    self.wxComplite?(nil, "登录失败")
                }
            } else {
                self.wxComplite?(nil, "登录失败")
            }
        }
    }
}
