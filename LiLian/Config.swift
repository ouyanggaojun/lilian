//
//  Config.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import UIKit
import SwiftyUserDefaults


let kWXAPPID = "wx3dddc9c1c4ee14c9"
let kUniversalLinks = "https://www.315jiage.cn/ios/"
let kWXAPPSECRET = "3b7fefe73ce7224eef68e4ceaef7efce"


let kshareTitle = "药品价格315网-网上药店价格查询,专业药品查询网站"
let kshareDesc = "专业的药品价格网站,提供及时准确的市场药价信息,合法网上药店价格对比,很方便的药品查询网站"
let kshareImg = "1024AppIcon".image()!

let knotificationName_LoginSuccess = "didLoginSuccess"
let knotificationName_DidLoginOut = "didLoginOut"


extension DefaultsKeys {
    var kcookie315: DefaultsKey<Dictionary<String, String>?> { .init("kcookie315") }
    var kcookieUser: DefaultsKey<Dictionary<String, Any>?> { .init("kcookieUser") }
    var knotificationCount: DefaultsKey<Int> { .init("knotificationCount", defaultValue: 1) }
}

