//
//  BaseViewController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit
import DropDown
import SwiftyFitsize
import swiftScan
import PPBadgeViewSwift
import SwiftyUserDefaults

class BaseViewController: UIViewController {
    
   lazy var webUrl:String = "https://www.315jiage.cn/"
    var shareTitle:String? {
        didSet {
            self.navigationItem.title = shareTitle
        }
    }
    var shareDes:String?
    
    var showRightItems:Bool = true {
        didSet {
            if showRightItems {
                self.navigationItem.rightBarButtonItems = self.naviBarItems.rightItems
            } else {
                self.navigationItem.rightBarButtonItems = []
            }
        }
    }
    lazy var naviBarItems : HomeViewControllerHelper = {
        let itemView = HomeViewControllerHelper()
        itemView.delegate = self
        return itemView
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        let item = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        self.navigationItem.backBarButtonItem = item;
    }
    
    @objc func layoutUI(){
        self.showRightItems = true
    }
    
    var backBtn: UIButton = {
        //设置返回按钮属性
        let backBtn = UIButton(type:.custom)
        let image = UIImage.init(iconfont: IconFont.back, iconColor: MainColor, size: CGSize.init(width: 28, height: 28))
        backBtn.setImage(image, for: .normal)
        backBtn.setImage(image, for: .highlighted)
        backBtn.titleLabel?.isHidden = true
        backBtn.addTarget(self, action:#selector(popController), for: .touchUpInside)
        backBtn.contentHorizontalAlignment = .left
        backBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: -5,bottom: 0,right: 0)
        let btnW:CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backBtn.frame = CGRect(x: 0,y: 0, width: btnW, height: 40)
        return backBtn
    }()
    
    @objc func popController() {
        navigationController?.popViewController(animated: true)
    }
   
    
    @objc func search() {
        let VC = PersonCenterController()
        VC.webUrl += "/appSearch.aspx"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @objc func scan() {
        let vc = ScanViewController()
        var style = LBXScanViewStyle()
        style.animationImage = "qrcode_scan_light_green".image()
        vc.scanStyle = style
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func share(sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.cellHeight = 50
        dropDown.width = 250
        dropDown.cellNib = UINib(nibName: "HomeDropDownCell", bundle: Bundle.main)
        dropDown.dataSource = ["朋友圈", "微信好友", "更多"]
        dropDown.customCellConfiguration = { (index, text, cell) in
            let homecell:HomeDropDownCell = cell as! HomeDropDownCell
            if text == "朋友圈" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.weixin, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            } else if text == "微信好友" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.pengyouquan, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "更多" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.gengduo2, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, text: String) in
            dropDown.hide()
            if text == "朋友圈" {
                WXSdkManager.share.sendLinkURL(webUrl, tagName: "", title: shareTitle ?? kshareTitle, description: shareDes ?? kshareDesc, thumbImage: kshareImg, in: WXSceneTimeline, complite: { (result, msg) in
                })
            } else if text == "微信好友" {
                WXSdkManager.share.sendLinkURL(webUrl, tagName: "", title: shareTitle ?? kshareTitle, description: shareDes ?? kshareDesc, thumbImage:kshareImg, in: WXSceneSession, complite: { (result, msg) in
                })
            }
            else if text == "更多" {
                let objectsToShare = [kshareTitle, releaseUrlWWW]
                let activityVC = UIActivityViewController(activityItems: objectsToShare as [Any], applicationActivities: nil)
                let excludedActivities = [UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.message, UIActivity.ActivityType.mail, UIActivity.ActivityType.print, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.saveToCameraRoll, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToTencentWeibo]
                activityVC.excludedActivityTypes = excludedActivities
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        dropDown.show()
    }
    
    @objc func more(sender: UIButton) {
        
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.cellHeight = 45
        dropDown.cellNib = UINib(nibName: "HomeDropDownCell", bundle: Bundle.main)
        if LoginManager.hasLogin {
            var array:[String] = ["\(LoginManager.share.loginUser.name)","绑定微信", "我的通知", "我的订单", "我的咨询", "我的服务", "退出登录", "关于"]
            if LoginManager.share.loginUser.wx == "1" {
                array.remove(at: 1)
            }
            dropDown.dataSource = array
        } else {
            dropDown.dataSource = ["登录", "注册", "关于"]
        }
        dropDown.customCellConfiguration = { (index, text, cell) in
            let homecell:HomeDropDownCell = cell as! HomeDropDownCell
            if text == "\(LoginManager.share.loginUser.name)" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.person, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            } else if text == "关于" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.guanyu, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            } else if text == "登录" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.person, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "退出登录" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.tuichu, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "注册" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.person, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "绑定微信" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.yunguanjia, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "我的通知" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.weixian, iconColor: MainColor, size: CGSize(width: 30, height: 30))
                let count = Defaults[\.knotificationCount]
                if count > 0 {
                    homecell.iconImageV?.pp.addDot(color: .red)
                    homecell.iconImageV.pp.moveBadge(x: -5, y: 5)
                } else {
                    homecell.optionLabel?.pp.hiddenBadge()
                }
            }
            else if text == "我的订单" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.liebiao, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "我的咨询" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.yijianfankui, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
            else if text == "我的服务" {
                homecell.iconImageV.image = UIImage(iconfont: IconFont.shuben, iconColor: MainColor, size: CGSize(width: 30, height: 30))
            }
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, text: String) in
            dropDown.hide()
            if text == "\(LoginManager.share.loginUser.name)" {
                let VC = PersonCenterController()
                VC.webUrl += "/mProfile.aspx"
                navigationController?.pushViewController(VC, animated: true)
            } else if text == "关于" {
                let VC = AboutAppController()
                navigationController?.pushViewController(VC, animated: true)
            } else if text == "登录" {
                let VC = LoginViewController()
                navigationController?.pushViewController(VC, animated: true)
            }
            else if text == "退出登录" {
                LoginManager.loginOut()
            }
            else if text == "注册" {
                let VC = PersonCenterController()
                VC.webUrl += "/mRegister.aspx"
                navigationController?.pushViewController(VC, animated: true)
            }
            else if text == "绑定微信" {
                WXSdkManager.share.login(complite: {
                    (res,msg) in
                    if res != nil {
                        let code = res?["code"]!
                        let VC = PersonCenterController()
                        VC.webUrl = "https://www.315jiage.cn/mWxLogin.aspx?bind=1&appcode=\(code ?? "")"
                        navigationController?.pushViewController(VC, animated: true)
                    }
                })
            }
            else if text == "我的通知" {
                Defaults[\.knotificationCount] = 0
                let VC = PersonCenterController()
                VC.webUrl += "/mMemberNotifications.aspx"
                navigationController?.pushViewController(VC, animated: true)
            }
            else if text == "我的订单" {
                let VC = PersonCenterController()
                VC.webUrl += "/mMemberOrders.aspx"
                navigationController?.pushViewController(VC, animated: true)
            }
            else if text == "我的咨询" {
                let VC = PersonCenterController()
                VC.webUrl += "/mMemberConsultation.aspx"
                navigationController?.pushViewController(VC, animated: true)
            }
            else if text == "我的服务" {
                let VC = PersonCenterController()
                VC.webUrl += "/mMemberCenter.aspx"
                navigationController?.pushViewController(VC, animated: true)
            }
        }
        dropDown.show()
    }
}


extension BaseViewController: HomeViewControllerHelperDelegate {
    func rightItemsDidClick(sender: UIButton) {
        switch sender.tag {
        case 10:
            more(sender:sender)
        case 11:
            share(sender:sender)
        case 12:
            scan()
        case 13:
            search()
        default: break
            
        }
    }
}
