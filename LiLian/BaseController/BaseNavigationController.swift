//
//  BaseNavigationController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/24.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    var backBtn: UIButton = {
        //设置返回按钮属性
        let backBtn = UIButton(type:.custom)
        let image = UIImage.init(iconfont: IconFont.back, iconColor: MainColor, size: CGSize.init(width: 28, height: 28))
        backBtn.setImage(image, for: .normal)
        backBtn.setImage(image, for: .highlighted)
        backBtn.titleLabel?.isHidden = true
        backBtn.addTarget(self, action:#selector(popController), for: .touchUpInside)
        backBtn.contentHorizontalAlignment = .left
        backBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: -5,bottom: 0,right: 0)
        let btnW:CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backBtn.frame = CGRect(x: 0,y: 0, width: btnW, height: 40)
        return backBtn
    }()
    
    @objc func popController() {
        popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBarAppearence()
    }
    
    func setupNavBarAppearence() {
        // 设置导航栏默认的背景颜色
        WRNavigationBar.defaultNavBarBarTintColor = UIColor.init(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
        
        // 设置导航栏所有按钮的
        WRNavigationBar.defaultNavBarTintColor = UIColor.red
        WRNavigationBar.defaultNavBarTitleColor = UIColor.black
        // 统一设置导航栏样式
        //        WRNavigationBar.defaultStatusBarStyle = .lightContent
        // 如果需要设置导航栏底部分割线隐藏，可以在这里统一设置
        WRNavigationBar.defaultShadowImageHidden = true
    }
    
}
extension BaseNavigationController{
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        viewController.navigationItem.hidesBackButton = true
        if children.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
            
            UINavigationBar.appearance().backItem?.hidesBackButton = true
            
            if viewController .isKind(of: BaseViewController.self) || viewController .isKind(of: BaseWebController.self)  {
                let VC:BaseViewController = viewController as! BaseViewController
                VC.navigationItem.leftBarButtonItem = UIBarButtonItem(customView:VC.backBtn);
            } else {
                viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(customView:backBtn);
            }
        }
        super.pushViewController(viewController, animated: animated)
    }
}

