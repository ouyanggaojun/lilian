//
//  BaseTabBarController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/24.
//

import UIKit
import ESTabBarController_swift
import Hue

class BaseTabBarController: ESTabBarController, UITabBarControllerDelegate {
    
    private lazy var itemTitles:Array<String> = {
        let array = ["首页", "购物车", "问药师", "问医生", "我的收益"]
        return array
    }()
    
    private lazy var itemImages:Array<UIImage> = {
        let array = [
            UIImage(iconfont: IconFont.shouye, iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.gouwuche, iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.yaodian, iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.yisheng , iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.meiyuan , iconColor: MainColor, size: CGSize(width: 25, height: 25))]
        return array
    }()
    
    private lazy var itemImagesSelect:Array<UIImage> = {
        let array = [
            UIImage(iconfont: IconFont.shouye, iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.gouwuche, iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.yaodian, iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.yisheng , iconColor: MainColor, size: CGSize(width: 25, height: 25)),
            UIImage(iconfont: IconFont.meiyuan , iconColor: MainColor, size: CGSize(width: 25, height: 25))]
        return array
    }()
    
    private lazy var itemControllers:Array<UIViewController> = {
        let array = [HomeViewContoller(), ShopViewController(), AskPharmacistController(), AskDoctorController(), MeController()]
        return array
    }()
    
    private lazy var itemNavControllers:Array<UINavigationController> = {
        let array = Array<UINavigationController>()
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarStyle()
    }
    
    /// 1.加载tabbar样式
    ///
    /// - Parameter delegate: 代理
    /// - Returns: ESTabBarController
    func setupTabBarStyle() {
        self.delegate = self
        self.title = "Irregularity"
        self.tabBar.shadowImage = UIImage(named: "transparent")
        
        for controller in itemControllers {
            let index = itemControllers.firstIndex(of: controller)!
            let contentView = ESTabBarItemContentView()
            contentView.itemContentMode = .alwaysOriginal
            contentView.textColor = UIColor.init(hex:"#337ab7")
            contentView.highlightTextColor = UIColor.init(hex:"#337ab7")
            contentView.iconColor = UIColor.init(hex:"#337ab7")
            contentView.highlightIconColor = UIColor.init(hex:"#337ab7")
            contentView.titleLabel.font = .systemFont(ofSize: 12)
            
            let item = ESTabBarItem.init(contentView, title:itemTitles[index], image: itemImages[index], selectedImage:itemImagesSelect[index])
            controller.tabBarItem = item
            
            controller.title = itemTitles[index]
            let nav = BaseNavigationController.init(rootViewController: controller)
            itemNavControllers.append(nav)
        }
        self.viewControllers = itemNavControllers
    }
}
