//
//  BaseViewController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import UIKit
import WebKit
import WKWebViewJavascriptBridge
import DropDown
import SwiftyFitsize
import swiftScan
import PPBadgeViewSwift
import SwiftyUserDefaults

enum WebLoadStatue {
    case start
    case success
    case fail
    case cancel
}

class BaseWebController: BaseViewController {
    var topHeight:CGFloat = 5;
    var loadStatue: WebLoadStatue?{
        didSet {
            updateProgress(progress: self.wkWebView.estimatedProgress);
            switch loadStatue {
            case .start:
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            default:
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    var cookies:Array<HTTPCookie>?
    
    lazy var jsArray:Array<String> = {
        var array = Array<String>()
        
        //隐藏头部
        array.append("""
                      var elemOld = document.getElementsByClassName('navbar navbar-default navbar-static-top')[0];
                      var elemParent = elemOld.parentNode;
                      var elemNew = document.createElement('div');
                          elemNew.style.height = '\(topHeight)px';
                      elemParent.replaceChild(elemNew, elemOld);
                """)
        
        //隐藏尾部
        array.append("document.getElementsByClassName('copyright')[0].style.display = 'none'")
        array.append("document.getElementsByClassName('navbar navbar-default navbar-fixed-bottom')[0].style.display = 'none'")
        return array
    }()
    
    lazy var progressView: UIProgressView = {
        let progress = UIProgressView()
        progress.transform = CGAffineTransform(scaleX: 1.0, y: 2.0)
        progress.tintColor = MainColor
        progress.progress = 0.0
        return progress
    }()
    
    func updateProgress(progress:Double) {
        if progress >= 1.0 {
            progressView.isHidden = true
            progressView.progress = 1.0
        } else {
            switch loadStatue {
            case .start:
                progressView.isHidden = false
                progressView.progress = Float(progress)
                
            case .fail:
                progressView.isHidden = false
                progressView.progress = 0.8
            
            case .success, .cancel:
                progressView.isHidden = true
                progressView.progress = 0.0
                
            default:
                break
            }
        }
    }
    
    lazy var wkWebView:WKWebView = {
        //配置环境
        let configuration = WKWebViewConfiguration()
        let userContentController = WKUserContentController()
        configuration.userContentController = userContentController;
        
        let webView = WKWebView(frame: CGRect.zero, configuration: configuration)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.clipsToBounds = true
        return webView
    }()
    
    lazy var webViewBridge : WKWebViewJavascriptBridge = {
        let webjs = WKWebViewJavascriptBridge(webView: wkWebView)
        return webjs
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
        setWebCookie(userContent: wkWebView.configuration.userContentController)
        reloadWebView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loginStatueChange), name: NSNotification.Name(rawValue: knotificationName_LoginSuccess), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginStatueChange), name: NSNotification.Name(rawValue: knotificationName_DidLoginOut), object: nil)
    }
    
    @objc func loginStatueChange() {
        self.reloadWebView()
    }
    
    override func layoutUI(){
        super.layoutUI()
        view.addSubview(wkWebView)
        view.addSubview(progressView)
        wkWebView.snp.remakeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(kHeight_NaviBarAndStatus)
            make.bottom.equalToSuperview().offset(-kHeight_BottomSafeArea)
        }
        progressView.snp.remakeConstraints { (make) in
            make.top.equalTo(wkWebView)
            make.left.right.equalTo(wkWebView)
            make.height.equalTo(2.0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        wkWebView.reload()
    }
    
    func reloadWebView() {
        self.wkWebView.load(URLRequest(url: URL(string: webUrl)!))
    }
    
    override func popController() {
        if wkWebView.canGoBack {
            wkWebView.goBack()
        } else {
            super.popController()
        }
    }
}

extension BaseWebController : WKNavigationDelegate, WKUIDelegate{
    
    // 开始加载页面
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loadStatue = .start
    }
    
    // 开始返回页面内容
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        webView.isHidden = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.loadStatue = .success
        
        self.shareTitle = webView.title
        // 执行网页元素操作
        for jsString in jsArray {
            webView.evaluateJavaScript(jsString) { (res, error) in
            };
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            webView.isHidden = false
        }
    }
    // 加载失败
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        loadStatue = .fail
    }
    
    // 页面跳转处理
    // 接收到服务器跳转请求之后调用
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
    }
    // 在收到响应后，决定是否跳转
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        Utils.addUserAgent("", webView: webView)
        //保存Cookie
        if #available(iOS 11, *) {
            webView.configuration.websiteDataStore.httpCookieStore.getAllCookies({ (cookies) in
                var cookieDictionary = [String:String]()
                for cookie in cookies {
                    if cookie.name == "iwmsUser" {
                        let decodedValue = cookie.value.decoded()
                        cookieDictionary[cookie.name] = decodedValue
                        LoginManager.share.didGetCookie(cookieString: decodedValue)
                    } else {
                        cookieDictionary[cookie.name] = cookie.value
                    }
                }
                Defaults[\.kcookie315] = cookieDictionary
            })
        }
        
        if navigationAction.request.url?.absoluteString == releaseUrlWWW + "/" {
            decisionHandler(.cancel)
            self.loadStatue = .cancel
            navigationController?.popToRootViewController(animated: true)
            (UIApplication.shared.delegate as! AppDelegate).tabBarController.selectedIndex = 0
            
        } else {
            let from = (webView.url?.absoluteString.containsIgnoringCase(find: "/mWxLogin.aspx?"))!
            let target = (navigationAction.request.url?.absoluteString.containsIgnoringCase(find: "/mWxLogin.aspx?"))!
            if !from && target {
                decisionHandler(.cancel)
                WXSdkManager.share.login(complite: {
                    [unowned self] (res,msg) in
                    if res != nil {
                        let code = res?["code"]!
                        if LoginManager.hasLogin {
                            self.webUrl = "https://www.315jiage.cn/mWxLogin.aspx?bind=1&appcode=\(code ?? "")"
                        } else {
                            self.webUrl = "https://www.315jiage.cn/mWxLogin.aspx?appcode=\(code ?? "")"
                        }
                        Utils.addUserAgent("DWD_HSQ/cn.jiage315.chayao", webView: webView)
                        self.reloadWebView()
                    }
                })
            } else {
                decisionHandler(.allow)
            }
        }
    }
    
    // 添加cookie
    func setWebCookie(userContent:WKUserContentController) {
        guard let cookieDictionary = Defaults[\.kcookie315] else {
            return
        }
        var cookieString = ""
        for cookie in cookieDictionary {
            cookieString.append("document.cookie =" + "'" + cookie.key + "=" + cookie.value + ";")
        }
        
        let cookieScript = WKUserScript(source: cookieString, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
        userContent.addUserScript(cookieScript)
    }
    
    // 处理_blank的链接请求
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        let frameInfo = navigationAction.targetFrame;
        if (!frameInfo!.isMainFrame) {
            webView.load(navigationAction.request);
        }
        return nil
    }
}
