//
//  AppDelegate.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WXApiDelegate {
    var window:UIWindow?
    lazy var tabBarController: BaseTabBarController = {
        let tabBarController = BaseTabBarController()
        return tabBarController
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        // 1.加载tabbar样式
        self.window?.backgroundColor = UIColor.white
        self.window?.rootViewController = tabBarController
        self.window?.makeKeyAndVisible()
        
        LoginManager.share.getLoginInfo()
            
        
        initThirdPart()
        return true
    }
    
    
    func initThirdPart() {
      let result = WXApi.registerApp(kWXAPPID, universalLink: kUniversalLinks)
        print("微信SDK init result: \(result)")
        WXApi.startLog(by: .detail) { (log) in
            print(#line,log)
        }
    }
}


extension AppDelegate {
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return WXApi.handleOpen(url, delegate: WXSdkManager.share)
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return WXApi.handleOpen(url, delegate: WXSdkManager.share)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return WXApi.handleOpenUniversalLink(userActivity, delegate: WXSdkManager.share)
    }
}
