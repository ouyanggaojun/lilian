//
//  Const.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import UIKit
import Kingfisher

extension UIDevice {
    static public func isiPhoneXMore() -> Bool {
        var isMore:Bool = false
        if #available(iOS 11.0, *) {
            isMore = (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)! > 0.0
        }
        return isMore
    }
    
    static func appVersion()-> String{
        let infoDictionary = Bundle.main.infoDictionary
        _ = infoDictionary!["CFBundleDisplayName"] as! String
        let majorVersion = infoDictionary! ["CFBundleShortVersionString"] as! String
        let minorVersion = infoDictionary! ["CFBundleVersion"] as! String
        return majorVersion + "." + minorVersion
    }
    
    static func removeCache() {
        // 取出cache文件夹路径.如果清除其他位子的可以将cachesDirectory换成对应的文件夹
        let cachePath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last
        // 打印路径,需要测试的可以往这个路径下放东西
        //print(cachePath)
        // 取出文件夹下所有文件数组
        let files = FileManager.default.subpaths(atPath: cachePath!)
        // 点击确定时开始删除
        for p in files!{
            // 拼接路径
            let path = cachePath!.appendingFormat("/\(p)")
            // 判断是否可以删除
            if FileManager.default.fileExists(atPath: path){
                // 删除
                //  try! FileManager.default.removeItem(atPath: path)
                /*******/
                //避免崩溃
                do {
                    try FileManager.default.removeItem(atPath: path as String)
                } catch {
                    print("removeItemAtPath err"+path)
                }
            }
        }
    }
}


//屏高
let kScreenHeight = UIScreen.main.bounds.size.height

//屏宽
let kScreenWidth = UIScreen.main.bounds.size.width

//iPhonex以上判断
let IS_IPhoneX_All = UIDevice.isiPhoneXMore()

//状态栏高
let kHeight_StatusBar = (IS_IPhoneX_All ? 44.0 : 20.0)

//导航栏高
let kHeight_NaviBar = 44.0

//导航栏高 + 状态栏
let kHeight_NaviBarAndStatus = kHeight_StatusBar + kHeight_NaviBar

//选项卡高
let kHeight_BottomSafeArea = (IS_IPhoneX_All ? 34.0 : 0)

//选项卡高
let kHeight_TabBar = 49.0

//安全区高
let kHeight_TabBarAndSafeBottom = kHeight_TabBar + kHeight_BottomSafeArea

//宽度比例
func kScaleWidth(_ font:CGFloat) -> (CGFloat) {
    return (kScreenWidth/375.0)*font
}

//高度比例
func kcaleHeight(_ font:CGFloat) -> (CGFloat) {
    return kScreenHeight/667.0*font
}

//字体比例
func kScaleFont(_ font:CGFloat) -> (CGFloat) {
    return kScreenWidth/375.0*font
}



// UI
let LineColor = UIColor.init(rgbHexString: "#E7E7E7")!

let MainColor = UIColor.init(rgbHexString: "#337ab7")!




