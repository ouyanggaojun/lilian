//
//  EWVerificationCodeController.h
//  ShowMe
//
//  Created by oygj on 2018/7/2.
//  Copyright © 2018年 yiwu. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^HWTimerHandler)(id userInfo);

/**解决 NSTimer 引用 target , NSRunroop 引用 timer , target不释放问题*/
@interface EWWeakTimer : NSObject

+ (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)interval
                                      target:(id)aTarget
                                    selector:(SEL)aSelector
                                    userInfo:(id)userInfo
                                     repeats:(BOOL)repeats;

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)interval
                                      block:(HWTimerHandler)block
                                   userInfo:(id)userInfo
                                    repeats:(BOOL)repeats;

@end
