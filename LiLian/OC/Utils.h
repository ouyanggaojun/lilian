//
//  Utils.h
//  LiLian
//
//  Created by ouyanggaojun on 2021/1/1.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject

+ (unsigned long long)convert36HexStrToDecimalWith36HexStr:(NSString *)str;

+ (NSString *)convertDecimalTo36HexStr:(unsigned long long)decimal;

+ (void)addUserAgent:(NSString *)addAgent webView:(WKWebView *)webView;

@end

NS_ASSUME_NONNULL_END
