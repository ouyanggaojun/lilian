//
//  MBProgressHUD+Extension.m
//  LiLian
//
//  Created by ouyanggaojun on 2021/1/1.
//

#import "MBProgressHUD+Extension.h"


#define EmitterColor_Red      [UIColor colorWithRed:255/255.0 green:0 blue:139/255.0 alpha:1]
#define EmitterColor_Yellow   [UIColor colorWithRed:251/255.0 green:197/255.0 blue:13/255.0 alpha:1]
#define EmitterColor_Blue     [UIColor colorWithRed:50/255.0 green:170/255.0 blue:207/255.0 alpha:1]


@interface EXColouredRibbonAnimation : NSObject

+ (void)ex_showSuccessColouredRibbonAnimationWithHideTime:(NSTimeInterval)time;

@end

@implementation EXColouredRibbonAnimation

+ (void)ex_showSuccessColouredRibbonAnimationWithHideTime:(NSTimeInterval)time {
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    // 后台背景
    UIView *backgroundView = [[UIView alloc] initWithFrame:window.bounds];
    backgroundView.backgroundColor = [UIColor clearColor];
    [window addSubview:backgroundView];

    double delayInSeconds = time;
    dispatch_time_t delayInNanoSeconds = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(delayInNanoSeconds, dispatch_get_main_queue(), ^(void){
        
        [backgroundView removeFromSuperview];

    });
    
    //开始粒子效果
    CAEmitterLayer *emitterLayer = addEmitterLayer(backgroundView);
    startAnimate(emitterLayer);

    
}

CAEmitterLayer *addEmitterLayer(UIView *view) {
    
    //色块粒子
    CAEmitterCell *subCell1 = subCell(imageWithColor(EmitterColor_Red));
    subCell1.name = @"red";
    CAEmitterCell *subCell2 = subCell(imageWithColor(EmitterColor_Yellow));
    subCell2.name = @"yellow";
    CAEmitterCell *subCell3 = subCell(imageWithColor(EmitterColor_Blue));
    subCell3.name = @"blue";
    CAEmitterCell *subCell4 = subCell([UIImage imageNamed:@"MBProgressExtension.bundle/success_star"]);
    subCell4.name = @"star";
    
    CAEmitterLayer *emitterLayer = [CAEmitterLayer layer];
    // 发射位置
    emitterLayer.emitterPosition = view.center;
    //    emitterLayer.emitterPosition = window.center;
    //发射源的尺寸大小
    emitterLayer.emitterSize    = view.bounds.size;
    //发射模式
    emitterLayer.emitterMode    = kCAEmitterLayerOutline;
    //发射源的形状
    emitterLayer.emitterShape    = kCAEmitterLayerRectangle;
    emitterLayer.renderMode        = kCAEmitterLayerOldestFirst;
    
    emitterLayer.emitterCells = @[subCell1,subCell2,subCell3,subCell4];
    [view.layer addSublayer:emitterLayer];
    
    return emitterLayer;
    
}

void startAnimate(CAEmitterLayer *emitterLayer)
{
    // birthRate粒子参数的速度乘数因子
    CABasicAnimation *redBurst = [CABasicAnimation animationWithKeyPath:@"emitterCells.red.birthRate"];
    redBurst.fromValue        = [NSNumber numberWithFloat:30];
    redBurst.toValue            = [NSNumber numberWithFloat:  0.0];
    redBurst.duration        = 0.5;
    redBurst.timingFunction    = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    CABasicAnimation *yellowBurst = [CABasicAnimation animationWithKeyPath:@"emitterCells.yellow.birthRate"];
    yellowBurst.fromValue        = [NSNumber numberWithFloat:30];
    yellowBurst.toValue            = [NSNumber numberWithFloat:  0.0];
    yellowBurst.duration        = 0.5;
    yellowBurst.timingFunction    = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    CABasicAnimation *blueBurst = [CABasicAnimation animationWithKeyPath:@"emitterCells.blue.birthRate"];
    blueBurst.fromValue        = [NSNumber numberWithFloat:30];
    blueBurst.toValue            = [NSNumber numberWithFloat:  0.0];
    blueBurst.duration        = 0.5;
    blueBurst.timingFunction    = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    CABasicAnimation *starBurst = [CABasicAnimation animationWithKeyPath:@"emitterCells.star.birthRate"];
    starBurst.fromValue        = [NSNumber numberWithFloat:30];
    starBurst.toValue            = [NSNumber numberWithFloat:  0.0];
    starBurst.duration        = 0.5;
    starBurst.timingFunction    = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[redBurst,yellowBurst,blueBurst,starBurst];
    
    [emitterLayer addAnimation:group forKey:@"heartsBurst"];
}

CAEmitterCell *subCell(UIImage *image)
{
    CAEmitterCell * cell = [CAEmitterCell emitterCell];
    
    cell.name = @"heart";
    cell.contents = (__bridge id _Nullable)image.CGImage;
    
    // 缩放比例
    cell.scale      = 0.6;
    cell.scaleRange = 0.6;
    // 每秒产生的数量
    //    cell.birthRate  = 40;
    cell.lifetime   = 20;
    // 每秒变透明的速度
    //    snowCell.alphaSpeed = -0.7;
    //    snowCell.redSpeed = 0.1;
    // 秒速
    cell.velocity      = 200;
    cell.velocityRange = 200;
    cell.yAcceleration = 9.8;
    cell.xAcceleration = 0;
    //掉落的角度范围
    cell.emissionRange  = M_PI;
    
    cell.scaleSpeed        = -0.05;
    ////    cell.alphaSpeed        = -0.3;
    cell.spin            = 2 * M_PI;
    cell.spinRange        = 2 * M_PI;
    
    return cell;
}

UIImage *imageWithColor(UIColor *color)
{
    CGRect rect = CGRectMake(0, 0, 13, 17);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end


@interface EXSuccessView : UIView<CAAnimationDelegate>

/**
 *  操作成功还是失败类型的动画
 */
@property(nonatomic, assign) EXAnimationType ex_animationType;

@end



@implementation EXSuccessView {
    CAShapeLayer *_shapeLayer;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {

    }
    return self;
}



- (void)setEx_animationType:(EXAnimationType)ex_animationType {
    _ex_animationType = ex_animationType;
    
    [self drawSuccessErrorLine];
}



- (void)drawSuccessErrorLine{

    //半径
    CGFloat radius = self.frame.size.width / 2;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    if (_ex_animationType == EXAnimationTypeSuccess) {
        [path moveToPoint:CGPointMake(self.center.x - radius + 5, self.center.y)];
        [path addLineToPoint:CGPointMake(self.center.x - 3.0f, self.center.y + 10.0f)];
        [path addLineToPoint:CGPointMake(self.center.x + radius - 5, self.center.y - 10)];
    } else if (_ex_animationType == EXAnimationTypeError) {
        [path moveToPoint:CGPointMake(radius / 2, radius / 2)];
        [path addLineToPoint:CGPointMake(radius + radius / 2, radius + radius / 2)];
        [path moveToPoint:CGPointMake(radius / 2, radius + radius / 2)];
        [path addLineToPoint:CGPointMake(radius + radius / 2, radius / 2)];
    }

    
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.path = path.CGPath;
    //线条颜色
    _shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    //填充颜色
    _shapeLayer.fillColor = [UIColor clearColor].CGColor;
    _shapeLayer.lineWidth = 5.0;
    // 设置线帽为圆
    _shapeLayer.lineCap = @"round";
    _shapeLayer.strokeStart = 0.0;
    // 该属性为 1.0 就是完成显示出来，可以通过给他动画就实现画线条出来。
    _shapeLayer.strokeEnd = 0.0;
    [self.layer addSublayer:_shapeLayer];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];

    [animation setFromValue:@0.0];
    [animation setToValue:@1.0];

    [animation setDuration:0.45];
   //当动画结束后,layer会一直保持着动画最后的状态
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    [_shapeLayer addAnimation:animation forKey:@"strokeEnd"];
    
}

@end

@implementation MBProgressHUD (EXExtension)

#pragma mark 显示一些信息
/**
 只显示文字
 */
+ (void)ex_showText:(NSString *)text view:(nullable UIView *)view
{
    [self ex_showPlainText:text hideAfterDelay:1.0 view:view];
}

+ (void)ex_showPlainText:(NSString *)text view:(nullable UIView *)view{
    [self ex_showPlainText:text hideAfterDelay:1.0 view:view];
}

+ (void)ex_showPlainText:(NSString *)text
          hideAfterDelay:(NSTimeInterval)time
                    view:(nullable UIView *)view {
    
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1秒之后再消失
    [hud hideAnimated:YES afterDelay:time];
    
}

#pragma mark 显示带有自定义icon图片的消息

+ (void)ex_showIcon:(UIImage *)icon
            message:(NSString *)message
     hideAfterDelay:(NSTimeInterval)time
               view:(nullable UIView *)view{
    
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // 默认
    //    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = message;
    // 设置图片
    hud.customView = [[UIImageView alloc] initWithImage:icon];
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1秒之后再消失
    [hud hideAnimated:YES afterDelay:time];

    
}

/**
 显示带有自定义icon图标消息HUD
 
 @param icon 图标
 @param message 消息正文
 @param view 展示的view
 */
+ (void)ex_showIcon:(UIImage *)icon message:(NSString *)message view:(nullable UIView *)view{
//    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
//    // 快速显示一个提示信息
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    // 默认
//    //    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.label.text = message;
//    // 设置图片
//    hud.customView = [[UIImageView alloc] initWithImage:icon];
//    // 再设置模式
//    hud.mode = MBProgressHUDModeCustomView;
//
//
//    // 隐藏时候从父控件中移除
//    hud.removeFromSuperViewOnHide = YES;
//
//    // 1秒之后再消失
//    [hud hide:YES afterDelay:1.0];
    
    [self ex_showIcon:icon message:message hideAfterDelay:1.0 view:view];
    
}


+ (void)ex_showIcon:(UIImage *)icon message:(NSString *)message{
    [self ex_showIcon:icon message:message view:nil];
}


#pragma mark 自定义View的方法

+ (void)ex_showCustomView:(UIView *)customView message:(nullable NSString *)message hideAfterDelay:(NSTimeInterval)time toView:(nullable UIView *)view
{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    if (message) {
        hud.label.text = message;
    }
    
    // 设置自定义view
    hud.customView = customView;
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 消失时间
    [hud hideAnimated:YES afterDelay:time];
}

+ (void)ex_showCustomView:(UIView *)customView hideAfterDelay:(NSTimeInterval)time toView:(nullable UIView *)view {
    [self ex_showCustomView:customView message:nil hideAfterDelay:time toView:view];
}

+ (void)ex_showCustomView:(UIView *)customView hideAfterDelay:(NSTimeInterval)time{
    [self ex_showCustomView:customView message:nil hideAfterDelay:time toView:nil];
}


+ (void)ex_showMessage:(nullable NSString *)message hideAfterDelay:(NSTimeInterval)time toView:(nullable UIView *)view customView:(UIView *(^)())customView{

    [self ex_showCustomView:customView() message:message hideAfterDelay:time toView:view];
    
}

+ (void)ex_showHideAfterDelay:(NSTimeInterval)time customView:(UIView *(^)())customView {
    
    [self ex_showCustomView:customView() hideAfterDelay:time toView:nil];
}

#pragma mark - 有加载进度的HUD

/**
 显示菊花加载状态
 
 @param message 消息正文
 @param view 展示的view
 */
+ (instancetype)ex_showActivityLoading:(nullable NSString *)message toView:(nullable UIView *)view
{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // 默认
    hud.mode = MBProgressHUDModeIndeterminate;
    
    if (message) {
        hud.label.text = message;
    }
    
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    return hud;

}

+ (instancetype)ex_showActivityLoadingToView:(nullable UIView *)view{
    return [self ex_showActivityLoading:nil toView:view];
}

+ (instancetype)ex_showActivityLoading{
    return [self ex_showActivityLoadingToView:nil];
}


+ (instancetype)ex_showAnnularLoading:(nullable NSString *)message toView:(nullable UIView *)view {
    
    return [self ex_showLoadingStyle:EXHUDLoadingStyleAnnularDeterminate
                             message:message
                              toView:view];

}

+ (instancetype)ex_showAnnularLoading {
    return [self ex_showAnnularLoading:nil toView:nil];
}



+ (instancetype)ex_showDeterminateLoading:(nullable NSString *)message toView:(nullable UIView *)view {
    
    return [self ex_showLoadingStyle:EXHUDLoadingProgressStyleDeterminate
                             message:message
                              toView:view];
    
}

+ (instancetype)ex_showDeterminateLoading {
    return [self ex_showDeterminateLoading:nil toView:nil];
}



+ (instancetype)ex_showLoadingStyle:(EXHUDLoadingProgressStyle)style message:(nullable NSString *)message toView:(nullable UIView *)view{
    
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
    if (style == EXHUDLoadingProgressStyleDeterminate) {
        hud.mode = MBProgressHUDModeDeterminate;
    } else if (style == EXHUDLoadingStyleDeterminateHorizontalBar) {
        hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
    } else if (style == EXHUDLoadingStyleAnnularDeterminate) {
        hud.mode = MBProgressHUDModeAnnularDeterminate;
    }
    
    if (message) {
        hud.label.text = message;
    }
    
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    return hud;
    
}

+ (instancetype)ex_showLoadingStyle:(EXHUDLoadingProgressStyle)style toView:(nullable UIView *)view {
    return [self ex_showLoadingStyle:style message:nil toView:view];
}

+ (instancetype)ex_showLoadingStyle:(EXHUDLoadingProgressStyle)style {
    return [self ex_showLoadingStyle:style toView:nil];
}




#pragma mark - 显示成功失败信息（打勾打叉动画）

+ (void)ex_showError:(nullable NSString *)error
      hideAfterDelay:(NSTimeInterval)time
              toView:(nullable UIView *)view{
    
    [self show:error animationType:EXAnimationTypeError hideAfterDelay:time view:view];
    
}

+ (void)ex_showError:(nullable NSString *)error toView:(nullable UIView *)view {
    
//    [self show:error animationType:EXAnimationTypeError view:view];
    [self ex_showError:error hideAfterDelay:1.0 toView:nil];
}

+ (void)ex_showError:(nullable NSString *)error
{
    [self ex_showError:error toView:nil];
}

+ (void)ex_showError{
    [self ex_showError:nil];
}



+ (void)ex_showSuccess:(nullable NSString *)success
        hideAfterDelay:(NSTimeInterval)time
                toView:(nullable UIView *)view{
    [self show:success animationType:EXAnimationTypeSuccess hideAfterDelay:time view:view];
}

+ (void)ex_showSuccess:(nullable NSString *)success toView:(nullable UIView *)view
{
    
//    [self show:success animationType:EXAnimationTypeSuccess view:view];
    [self ex_showSuccess:success hideAfterDelay:1.0 toView:view];

}

+ (void)ex_showSuccess:(nullable NSString *)success
{
    [self ex_showSuccess:success toView:nil];
}

+ (void)ex_showSuccess
{
    [self ex_showSuccess:nil];
}


+ (void)show:(nullable NSString *)text animationType:(EXAnimationType)animationType hideAfterDelay:(NSTimeInterval)time view:(nullable UIView *)view {
   
    EXSuccessView *suc = [[EXSuccessView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    suc.ex_animationType = animationType;
    [MBProgressHUD ex_showCustomView:suc message:text hideAfterDelay:time toView:view];
}
#pragma mark - 操作成功彩带粒子祝贺HUD

+ (void)ex_showSuccessWithColouredRibbonAnimation:(nullable NSString *)message
                                   hideAfterDelay:(NSTimeInterval)time {
    [self ex_showSuccess:message hideAfterDelay:time toView:nil];
    [EXColouredRibbonAnimation ex_showSuccessColouredRibbonAnimationWithHideTime:time];
}

+ (void)ex_showSuccessWithColouredRibbonAnimation:(nullable NSString *)message {
//    [self ex_showSuccess:message];
//    [EXColouredRibbonAnimation ex_showSuccessColouredRibbonAnimationWithHideTime:1.0];
    [self ex_showSuccessWithColouredRibbonAnimation:message hideAfterDelay:1.0];
    
}

+ (void)ex_showSuccessWithColouredRibbonAnimation {
    [self ex_showSuccessWithColouredRibbonAnimation:nil];
    
}

+ (void)ex_showColouredRibbonAnimationWithHideTime:(NSTimeInterval)time{
    [EXColouredRibbonAnimation ex_showSuccessColouredRibbonAnimationWithHideTime:time];
}



#pragma mark 隐藏HUD

+ (void)ex_hideHUDForView:(nullable UIView *)view
{
    [self hideHUDForView:view animated:YES];
}

+ (void)ex_hideHUD
{
    [self ex_hideHUDForView:nil];
}

@end

