//
//  MBProgressHUD+Extension.h
//  LiLian
//
//  Created by ouyanggaojun on 2021/1/1.
//

#import <MBProgressHUD/MBProgressHUD.h>

// 过期提示
#define EXExtensionDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)
#define EXAttributeDeprecated(instead) __attribute__((deprecated(instead)))

typedef NS_ENUM(NSInteger, EXHUDLoadingProgressStyle) {
    /* 开扇型加载进度 */
    EXHUDLoadingProgressStyleDeterminate,
    /* 横条加载进度 */
    EXHUDLoadingStyleDeterminateHorizontalBar,
    /* 环形加载进度 */
    EXHUDLoadingStyleAnnularDeterminate,
};

typedef NS_ENUM(NSInteger, EXAnimationType) {
    EXAnimationTypeNone,
    /* 成功 */
    EXAnimationTypeSuccess,
    /* 失败 */
    EXAnimationTypeError,
};


@interface MBProgressHUD (EXExtension)


#pragma mark - 显示纯文本信息
/**
 只显示纯文本信息, 默认 1 秒后消失
 */
+ (void)ex_showText:(NSString *)text view:(nullable UIView *)view EXAttributeDeprecated("使用 ex_showPlainText:view:方法");

/**
 只显示纯文本信息, 默认 1 秒后消失
 @param text 消息文本
 @param view 展示的View
 */
+ (void)ex_showPlainText:(NSString *)text view:(nullable UIView *)view;


/**
 只显示纯文本信息
 @param text 消息文本
 @param time HUD展示时长
 @param view 展示的View
 */
+ (void)ex_showPlainText:(NSString *)text
          hideAfterDelay:(NSTimeInterval)time
                    view:(nullable UIView *)view;


#pragma mark - 显示操作成功或失败信息（自定义view打勾打叉动画）

/**
 显示失败信息，同时有打叉的动画
 
 @param error 错误信息
 @param time HUD展示时长
 @param view 展示的view
 */
+ (void)ex_showError:(nullable NSString *)error
      hideAfterDelay:(NSTimeInterval)time
              toView:(nullable UIView *)view;

/**
 显示失败信息，同时有打叉的动画, 默认 1 秒后消失
 @param error 错误信息提示文本
 @param view 展示的View
 */
+ (void)ex_showError:(nullable NSString *)error toView:(nullable UIView *)view;



/**
 显示失败信息，同时有打叉的动画，默认 1 秒后消失
 @param error 错误信息提示文本
 */
+ (void)ex_showError:(nullable NSString *)error;

/**
 只显示打叉动画HUD，默认 1 秒后消失
 */
+ (void)ex_showError;



/**
 显示成功信息，同时会有一个打勾的动画
 @param success 成功信息提示文本
 @param time HUD展示时长
 @param view 展示的View
 */
+ (void)ex_showSuccess:(nullable NSString *)success
      hideAfterDelay:(NSTimeInterval)time
              toView:(nullable UIView *)view;

/**
 显示成功信息，同时会有一个打勾的动画, 默认 1s 后消失
 @param success 成功信息提示文本
 @param view 展示的View
 */
+ (void)ex_showSuccess:(nullable NSString *)success toView:(nullable UIView *)view;
/**
 显示成功信息，同时会有一个打勾的动画, 默认 1s 后消失
 
 @param success 成功信息提示文本
 */
+ (void)ex_showSuccess:(nullable NSString *)success;

/**
 只显示打勾动画HUD，默认 1s 后消失
 */
+ (void)ex_showSuccess;



#pragma mark - 操作成功 & 彩带粒子碎花祝贺HUD效果

/**
 显示操作成功HUD的同时伴随碎花粒子动画效果，可用于祝贺的场景
 @param message 祝贺消息
 @param time HUD展示时长
 */
+ (void)ex_showSuccessWithColouredRibbonAnimation:(nullable NSString *)message
                        hideAfterDelay:(NSTimeInterval)time;

/**
 显示操作成功HUD的同时伴随碎花粒子动画效果，可用于祝贺的场景, 默认 1s 后消失
 @param message 祝贺消息
 */
+ (void)ex_showSuccessWithColouredRibbonAnimation:(nullable NSString *)message;


/**
 显示操作成功HUD的同时伴随碎花粒子动画效果，可用于祝贺的场景， 默认 1s 后消失
 */
+ (void)ex_showSuccessWithColouredRibbonAnimation;

#pragma mark - 自行调用彩带粒子碎花效果方法
/**
 显示碎花粒子效果
 */
+ (void)ex_showColouredRibbonAnimationWithHideTime:(NSTimeInterval)time;






#pragma mark - 自己设置提示信息的 图标

/**
 显示带有自定义icon图标消息HUD
 @param icon 图标
 @param message 消息正文
 @param time HUD展示时长
 @param view 展示的view
 */
+ (void)ex_showIcon:(UIImage *)icon
            message:(NSString *)message
     hideAfterDelay:(NSTimeInterval)time
               view:(nullable UIView *)view;

/**
 显示带有自定义icon图标消息HUD, 默认 1s 后消失
 @param icon 图标
 @param message 消息正文
 @param view 展示的view
 */
+ (void)ex_showIcon:(UIImage *)icon
            message:(NSString *)message
               view:(nullable UIView *)view;

+ (void)ex_showIcon:(UIImage *)icon message:(NSString *)message;


#pragma mark - 有加载进度的HUD


/**
 显示菊花加载状态，不会自动消失，需要在你需要移除的时候调用 ex_hideHUDForView: 等移除方法
 @param message 消息正文
 @param view 展示的view
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showActivityLoading:(nullable NSString *)message
                                toView:(nullable UIView *)view;

/**
 只显示菊花加载动画，不会自动消失，需要在你需要移除的时候调用 ex_hideHUDForView: 等移除方法
 @param view 展示的View
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showActivityLoadingToView:(nullable UIView *)view;
/**
 只显示菊花加载动画，不会自动消失，需要在你需要移除的时候调用 ex_hideHUDForView: 等移除方法
 
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showActivityLoading;


/**
 加载进度的HUD，设置HUD的progress请通过 HUD 对象调用 showAnimated: whileExecutingBlock: 等方法
 
 使用举例：
 MBProgressHUD *hud = [MBProgressHUD ex_showLoadingStyle:EXHUDLoadingProgressStyleDeterminate message:@"正在加载..." toView:nil];
 [hud showAnimated:YES whileExecutingBlock:^{
     float progress = 0.0f;
     while (progress < 1.0f) {
        hud.progress = progress;
        hud.labelText = [NSString stringWithFormat:@"正在加载...%.0f%%", progress * 100];
        progress += 0.01f;
        usleep(50000);
     }
 } completionBlock:^{
    [MBProgressHUD ex_hideHUD];
 // [hud removeFromSuperViewOnHide];
 }];
 
 @param style 进度条样式
 @param message 消息正文，传nil不显示
 @param view 展示的View
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showLoadingStyle:(EXHUDLoadingProgressStyle)style
                            message:(nullable NSString *)message
                             toView:(nullable UIView *)view;

/**
 只显示加载进度的HUD，不显示消息文本，设置HUD的progress请通过 HUD 对象调用 showAnimated: whileExecutingBlock: 等方法
 @param style 进度条样式
 @param view 展示的View
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showLoadingStyle:(EXHUDLoadingProgressStyle)style toView:(nullable UIView *)view;

/**
 只显示加载进度的HUD，不显示消息文本，设置HUD的progress请通过 HUD 对象调用 showAnimated: whileExecutingBlock: 等方法
 @param style 进度条样式
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showLoadingStyle:(EXHUDLoadingProgressStyle)style;



/**
 显示环形加载状态指示器
 @param message 消息正文，传nil不显示
 @param view 展示的View
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showAnnularLoading:(nullable NSString *)message
                               toView:(nullable UIView *)view;

/**
 只显示环形加载状态指示器，不显示文本消息
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showAnnularLoading;

/**
 扇形饼状加载进度
 
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showDeterminateLoading:(nullable NSString *)message
                                   toView:(nullable UIView *)view;

/**
 只显示扇形饼状加载进度指示器，不显示文本消息
 @return MBProgressHUD对象，可以通过它调用MBProgressHUD中的方法
 */
+ (instancetype)ex_showDeterminateLoading;



#pragma mark - 自定义HUD中显示的view

/**
 自定义HUD中显示的view
 @param customView 自定义的view
 @param message 消息正文，传nil只显示自定义的view在HUD上
 @param time HUD展示时长
 @param view 展示的view
 */
+ (void)ex_showCustomView:(UIView *)customView
                  message:(nullable NSString *)message
           hideAfterDelay:(NSTimeInterval)time
                     toView:(nullable UIView *)view;

+ (void)ex_showCustomView:(UIView *)customView
           hideAfterDelay:(NSTimeInterval)time
                     toView:(nullable UIView *)view;

+ (void)ex_showCustomView:(UIView *)customView
           hideAfterDelay:(NSTimeInterval)time;

/**
 自定义HUD中显示的view, 闭包返回自定义的View
 @param message 消息正文
 @param time HUD展示时长
 @param view 展示的view
 @param customView 返回自定义UIView
 */
+ (void)ex_showMessage:(nullable NSString *)message
        hideAfterDelay:(NSTimeInterval)time
                toView:(nullable UIView *)view
            customView:(UIView *(^)())customView;

+ (void)ex_showHideAfterDelay:(NSTimeInterval)time
                   customView:(UIView *(^)())customView;



#pragma mark - 移除HUD
/**
 从view上移除HUD
 @param view 展示HUD的View
 */
+ (void)ex_hideHUDForView:(nullable UIView *)view;
/**
 从当前展示的View上移除HUD
 */
+ (void)ex_hideHUD;


@end
