//
//  Utils.m
//  LiLian
//
//  Created by ouyanggaojun on 2021/1/1.
//

#import "Utils.h"


@implementation Utils

/**
 将36进制的字符串转为10进制的数字
 @param str 36进制的字符串
 @return 10进制的数字
 */
+ (unsigned long long)convert36HexStrToDecimalWith36HexStr:(NSString *)str {
    
    NSString *str36 = str.copy;
    NSString *param = @"0123456789abcdefghijklmnopqrstuvwxyz";
    unsigned long long num = 0;
    for (unsigned long long i = 0; i < str36.length; i++) {
        for (NSInteger j = 0; j < param.length; j++) {
            char iChar = [str36 characterAtIndex:i];
            char jChar = [param characterAtIndex:j];
            if (iChar == jChar) {
                unsigned long long n = j * pow(36, str36.length - i - 1);
                num += n;
                break;
            }
        }
    }
    return num;
}

/**
 10进制数字转换为36进制字符串
 
 @param decimal 10进制数字
 
 @return 36进制的字符串
 */
+ (NSString *)convertDecimalTo36HexStr:(unsigned long long)decimal {
    
    NSMutableString *dd = @"".mutableCopy;
    NSString* parma =@"0123456789abcdefghijklmnopqrstuvwxyz";
    unsigned long long i = decimal;
    while(i > 0) {
        int c = i % 36;
        i = i / 36;
        char cc = [parma characterAtIndex:c];
        [dd insertString:[NSString stringWithFormat:@"%c",cc] atIndex:0];
    }
    return dd;
}


+ (void)addUserAgent:(NSString *)addAgent webView:(WKWebView *)webView {
    [webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(NSString * oldAgent, NSError * _Nullable error) {
        
        oldAgent = [NSString stringWithFormat:@"Mozilla/5.0 (%@; CPU iPhone OS %@ like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Safari/604.1", [[UIDevice currentDevice] model], [[[UIDevice currentDevice] systemVersion] stringByReplacingOccurrencesOfString:@"." withString:@"_"]];
        
        //自定义user-agent
        if (addAgent) {
            NSString *newAgent = [oldAgent stringByAppendingFormat:@" %@",addAgent];
            [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent":newAgent}];
            // 一定要设置customUserAgent，否则执行navigator.userAgent拿不到oldAgent
            webView.customUserAgent = newAgent;
        } else {
            webView.customUserAgent = oldAgent;
        }
    }];
}

@end
