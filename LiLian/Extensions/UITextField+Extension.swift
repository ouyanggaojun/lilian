//
//  UITextField+DSL.swift
//  Alamofire
//
//  Created by xudongzhang on 2018/9/13.
//

import UIKit

public extension UITextField {
    @discardableResult
    func text(_ text: String) -> Self {
        self.text = text
        return self
    }
    @discardableResult
    func attributedText(_ text: NSAttributedString) -> Self {
        self.attributedText = text
        return self
    }
    

    @discardableResult
    func placeholder(_ text: String) -> Self {
        self.placeholder = text
        return self
    }
    @discardableResult
    func attributedPlaceholder(_ text: NSAttributedString) -> Self {
        self.attributedPlaceholder = text
        return self
    }

    @discardableResult
    func alignment(_ alignment: NSTextAlignment) -> Self {
        self.textAlignment = alignment
        return self
    }

    @discardableResult
    func color(_ color: UIColor) -> Self {
        self.textColor = color
        return self
    }

    @discardableResult
    func font(_ font: UIFont) -> Self {
        self.font = font
        return self
    }

    @discardableResult
    func font(_ fontSize: Float) -> Self {
        self.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        return self
    }

    @discardableResult
    func font(_ fontSize: Int) -> Self {
        self.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        return self
    }
    
    @discardableResult
    func delegate(_ delegate: UITextFieldDelegate) -> Self {
        self.delegate = delegate
        return self
    }

}
