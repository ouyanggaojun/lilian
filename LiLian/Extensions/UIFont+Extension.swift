//
//  UIFont+swift.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/28.
//

import UIKit
public extension UIFont {
    class func iconfont(ofSize: CGFloat) -> UIFont? {
        return UIFont(name: "iconfont", size: ofSize)
    }
}
