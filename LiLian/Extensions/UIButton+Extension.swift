//
//  UIButton+DSL.swift
//  Alamofire
//
//  Created by xudongzhang on 2018/9/13.
//

import UIKit
public extension UIButton {
    
    @discardableResult
    func title(_ text: String?, _ state: UIControl.State = .normal) -> Self {
        self.setTitle(text, for: state)
        return self
    }

    @discardableResult
    func color(_ color: UIColor?, _ state: UIControl.State = .normal) -> Self {
        self.setTitleColor(color, for: state)
        return self
    }

    @discardableResult
    func font(_ font: UIFont?) -> Self {
        self.titleLabel?.font = font
        return self
    }

    @discardableResult
    func font(_ fontSize: Float) -> Self {
        self.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        return self
    }

    @discardableResult
    func font(_ fontSize: Int) -> Self {
        self.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        return self
    }
    
    @discardableResult
    func boldFont(_ fontSize: Float) -> Self {
        self.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        return self
    }
    
    @discardableResult
    func boldFont(_ fontSize: Int) -> Self {
        self.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(fontSize))
        return self
    }

    @discardableResult
    func image(_ image: UIImage?, _ state: UIControl.State = .normal) -> Self {
        self.setImage(image, for: state)
        return self
    }

    @discardableResult
    func image(in bundle: Bundle? = nil, _ imageName: String, _ state: UIControl.State = .normal) -> Self {
        let image = UIImage(named: imageName, in: bundle, compatibleWith: nil)
        self.setImage(image, for: state)
        return self
    }

    @discardableResult
    func image(in bundle: Bundle, name imageName: String, _ state: UIControl.State = .normal) -> Self {
        let image = UIImage(named: imageName, in: bundle, compatibleWith: nil)
        self.setImage(image, for: state)
        return self
    }
    
    @discardableResult
    func image(for aClass: AnyClass, name imageName: String, _ state: UIControl.State = .normal) -> Self {
        let image = UIImage(named: imageName, in: Bundle(for: aClass), compatibleWith: nil)
        self.setImage(image, for: state)
        return self
    }
    
    @discardableResult
    func image(forParent aClass: AnyClass, bundleName: String, _ imageName: String, _ state: UIControl.State = .normal) -> Self {
        guard let path = Bundle(for: aClass).path(forResource: bundleName, ofType: "bundle") else {
            return self
        }
        let image = UIImage(named: imageName, in: Bundle(path: path), compatibleWith: nil)
        self.setImage(image, for: state)
        return self
    }
    

    @discardableResult
    func bgImage(_ image: UIImage?, _ state: UIControl.State = .normal) -> Self {
        self.setBackgroundImage(image, for: state)
        return self
    }
    
    @discardableResult
    func bgImage(forParent aClass: AnyClass, bundleName: String, _ imageName: String, _ state: UIControl.State = .normal) -> Self {
        guard let path = Bundle(for: aClass).path(forResource: bundleName, ofType: "bundle") else {
            return self
        }
        let image = UIImage(named: imageName, in: Bundle(path: path), compatibleWith: nil)
        self.setBackgroundImage(image, for: state)
        return self
    }

    @discardableResult
    func bgImage(in bundle: Bundle? = nil, _ imageName: String, _ state: UIControl.State = .normal) -> Self {
        let image = UIImage(named: imageName, in: bundle, compatibleWith: nil)
        self.setBackgroundImage(image, for: state)
        return self
    }

    @discardableResult
    func bgImage(for aClass: AnyClass, _ imageName: String, _ state: UIControl.State = .normal) -> Self {
        let image = UIImage(named: imageName, in: Bundle(for: aClass), compatibleWith: nil)
        self.setBackgroundImage(image, for: state)
        return self
    }
   
    @discardableResult
    func textColor(_ color: UIColor?, state: UIControl.State = .normal) -> Self {
        self.setTitleColor(color, for: state)
        return self
    }
}


