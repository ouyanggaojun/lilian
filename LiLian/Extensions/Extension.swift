//
//  Extension.swift
//  LiLian
//
//  Created by ouyanggaojun on 2021/1/4.
//

import UIKit

extension CGRect {
    
    var width: CGFloat {
        return self.size.width
    }
    
    var height: CGFloat {
        return self.size.height
    }
    
    var x: CGFloat {
        return self.origin.x
    }
    
    var y: CGFloat {
        return self.origin.y
    }
    
    var top: CGFloat {
        return self.origin.y
    }
    
    var bottom: CGFloat {
        return self.origin.y + self.size.height
    }
    
    var left: CGFloat {
        return self.origin.x
    }
    
    var right: CGFloat {
        return self.origin.x + self.size.width
    }
    
    var centre: CGPoint {
        return CGPoint(x: self.left + self.width/2, y: self.top + self.height/2)
    }
}
