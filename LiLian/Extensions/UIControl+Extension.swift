//
//  UIControl+DSL.swift
//  SwiftselfLib
//
//  Created by 0314 on 2019/5/10.
//

import Foundation
import UIKit
public extension UIControl {
    
    @discardableResult
    func isEnabled(_ isEnabled: Bool) -> Self {
        self.isEnabled = isEnabled
        return self
    }
    
    @discardableResult
    func isSelected(_ isSelected: Bool) -> Self {
        self.isSelected = isSelected
        return self
    }
    
    @discardableResult
    func isHighlighted(_ isSelected: Bool) -> Self {
        self.isHighlighted = isSelected
        return self
    }
    
    @discardableResult
    func contentVerticalAlignment(_ contentVerticalAlignment: UIControl.ContentVerticalAlignment) -> Self {
        self.contentVerticalAlignment = contentVerticalAlignment
        return self
    }
    
    @discardableResult
    func contentHorizontalAlignment(_ contentHorizontalAlignment: UIControl.ContentHorizontalAlignment) -> Self {
        self.contentHorizontalAlignment = contentHorizontalAlignment
        return self
    }
    
    @discardableResult
    func add(_ target: Any?, action: Selector, events: UIControl.Event = .touchUpInside) -> Self {
        self.addTarget(target, action: action, for: events)
        return self
    }
    
    @discardableResult
    func remove(_ target: Any?, action: Selector, events: UIControl.Event = .touchUpInside) -> Self {
        self.removeTarget(target, action: action, for: events)
        return self
    }
    
    @discardableResult
    func addAction(_ action: @escaping (UIControl) -> Void) -> Self {
        self.cui_action_block = action
        add(self, action: #selector(UIControl.cui_btn_action))
        return self
    }
}

fileprivate  extension UIControl {
    private static var cui_action_block_key = "cui_action_block_key"
    
    @objc func cui_btn_action() {
        cui_action_block?(self)
    }
    
    var cui_action_block: ((UIControl) -> ())? {
        get {
            return objc_getAssociatedObject(self, &UIControl.cui_action_block_key) as? ((UIControl) -> ())
        }
        set {
            guard let action = newValue else { return }
            objc_setAssociatedObject(self, &UIControl.cui_action_block_key, action, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
