//
//  DataExtensions.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/31.
//

import Foundation

extension Data {
    func toDictionary() -> Dictionary<String, Any>? {
        do {
            let json = try JSONSerialization.jsonObject(with: self, options: .mutableContainers)
            let dic = json as! Dictionary<String, Any>
            return dic
        } catch _ {
            print("失败")
            return nil
        }
    }
}

