//
//  CALayer+DSL.swift
//  SwiftselfLib
//
//  Created by ming on 2019/7/11.
//


//layer.cornerRadius 设置圆角弧度，设置成String/Number。
//layer.masksToBounds 设置裁剪，设置成Boolean
//layer.borderWidth 设置边框宽度， 设置成String/Number
//layer.borderColorFromUIColor 设置边框颜色，设置成Color

import UIKit

public extension CALayer {
    
    @discardableResult
    func corner(_ cornerRadius: CGFloat) -> Self {
        self.cornerRadius = cornerRadius
        self.masksToBounds = true
        return self
    }
    
    @discardableResult
    func backgroundColor(_ color: UIColor) -> Self {
        self.backgroundColor = color.cgColor
        return self
    }
    
    @discardableResult
    func backgroundColor(_ hex: Int) -> Self {
        self.backgroundColor = UIColor.hex(hex).cgColor
        return self
    }
    
    @discardableResult
    func frame(_ frame: CGRect) -> Self {
        self.frame = frame
        return self
    }
    
    @discardableResult
    func addTo(_ superView: UIView) -> Self {
        superView.layer.addSublayer(self)
        return self
    }
    
    @discardableResult
    func addTo(_ superLayer: CALayer) -> Self {
        superLayer.addSublayer(self)
        return self
    }
    
    @discardableResult
    func isHidden(_ isHidden: Bool) -> Self {
        self.isHidden = isHidden
        return self
    }
    
    @discardableResult
    func borderWidth(_ width: CGFloat) -> Self {
        self.borderWidth = width
        return self
    }
    
    @discardableResult
    func borderColor(_ color: UIColor) -> Self {
        self.borderColor = color.cgColor
        return self
    }
    
    @discardableResult
    func borderColorFromUIColor(_ color: UIColor) -> Self {
        self.borderColor = color.cgColor
        return self
    }
    
    /// 是否开启光栅化
    @discardableResult
    func shouldRasterize(_ rasterize: Bool) -> Self {
        self.shouldRasterize = rasterize
        return self
    }
    
    /// 开启光栅化比例
    @discardableResult
    func rasterizationScale(_ scale: CGFloat) -> Self {
        self.rasterizationScale = scale
        self.shouldRasterize = true
        return self
    }
    
    /// 阴影颜色
    @discardableResult
    func shadowColor(_ color: UIColor) -> Self {
        self.shadowColor = color.cgColor
        return self
    }
    
    /// 阴影的透明度
    @discardableResult
    func shadowOpacity(_ opacity: Float) -> Self {
        self.shadowOpacity = opacity
        return self
    }
    
    /// 阴影偏移量
    @discardableResult
    func shadowOffset(_ offset: CGSize) -> Self {
        self.shadowOffset = offset
        return self
    }
    /// 阴影圆角
    @discardableResult
    func shadowRadius(_ radius: CGFloat) -> Self {
        self.shadowRadius = radius
        return self
    }
    /// 阴影偏移量
    @discardableResult
    func shadowPath(_ path: CGPath) -> Self {
        self.shadowPath = path
        return self
    }
    
}

/// - Important: UI conveniently
public extension CALayer {
    var x: CGFloat {
        get { return self.frame.origin.x }
        set(newValue) {
            var tf: CGRect = self.frame
            tf.origin.x    = newValue
            self.frame          = tf
        }
    }
    var y: CGFloat {
        get { return self.frame.origin.y }
        set(newValue) {
            var tf: CGRect = self.frame
            tf.origin.y    = newValue
            self.frame          = tf
        }
    }
    var height: CGFloat {
        get { return self.frame.size.height }
        set(newValue) {
            var tf: CGRect = self.frame
            tf.size.height = newValue
            self.frame          = tf
        }
    }
    var width: CGFloat {
        get { return self.frame.size.width }
        set(newValue) {
            var tf: CGRect = self.frame
            tf.size.width  = newValue
            self.frame = tf
        }
    }
    var size: CGSize {
        get { return self.frame.size }
        set(newValue) {
            var tf: CGRect = self.frame
            tf.size        = newValue
            self.frame          = tf
        }
    }
    /// - Attention: try清除所有subviews，考虑多线程，故返回bool标记
    func clear() -> Bool {
        for v in self.sublayers ?? [] {
            v.removeFromSuperlayer()
        }
        return self.sublayers?.count == 0
    }
    
    func asImage() -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: self.bounds)
            return renderer.image { rendererContext in
                self.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, 0)
            guard let ctx = UIGraphicsGetCurrentContext() else {
                return nil
            }
            self.render(in: ctx)
            let resultImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return resultImage
        }
    }
}
