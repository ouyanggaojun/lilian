//
//  YYIconFont.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/28.
//

import UIKit
import CoreText

//MARK: - 扩展UILable
extension UILabel{
    ///直接通过UILabel加载iconfont
    /// - parameter iconfont: 图标的编码标准格式如："\u{a626}"
    convenience init(frame: CGRect,iconfont code:IconFont, fontSize:CGFloat) {
        self.init(frame: frame)
        self.text = code.rawValue
        self.font = UIFont(name: "IconFont", size: fontSize)
        self.textAlignment = .center
    }
}

//MARK: - 扩展UIImage
extension UIImage {
     
    /*
     //方法一：
     UIImage.init(from: iconFont, textColor: .black, size: CGSize.init(width: 25, height: 25))
     //方法二：
     UIImage.icon(from: iconFont, iconColor: .black, imageSize: CGSize.init(width: 25, height: 25), ofSize: 25)
     */
    convenience init(iconfont font: IconFont, iconColor: UIColor?, backgroundColor: UIColor = .clear, size: CGSize) {
        let drawText = font
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        let fontSize = min(size.width / 1.28571429, size.height)
        let attributes: [NSAttributedString.Key: Any] = [.font: UIFont.iconfont(ofSize: fontSize) ?? UIFont.systemFont(ofSize: 14), .foregroundColor: iconColor, .backgroundColor: backgroundColor, .paragraphStyle: paragraphStyle]
        
        let attributedString = NSAttributedString(string: drawText.rawValue, attributes: attributes)
        UIGraphicsBeginImageContextWithOptions(size, false , UIScreen.main.scale)
        attributedString.draw(in: CGRect(x: 0, y: (size.height - fontSize) * 0.5, width: size.width, height: fontSize))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let image = image {
            self.init(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)
        } else {
            self.init()
        }
    }
    
    static func icon(iconfont font: IconFont, iconColor: UIColor = .black, imageSize: CGSize, ofSize size: CGFloat) -> UIImage {
        let drawText = font.rawValue
        UIGraphicsBeginImageContextWithOptions(imageSize, false, UIScreen.main.scale)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.center
        
        drawText.draw(in: CGRect(x:0, y:0, width:imageSize.width, height:imageSize.height), withAttributes: [.font: UIFont.iconfont(ofSize: size) ?? UIFont.systemFont(ofSize: 14), .paragraphStyle: paragraphStyle, .foregroundColor: iconColor])
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

public enum IconFont: String {
    case back = "\u{e728}"
    case fenxiang = "\u{e612}"
    case yunguanjia = "\u{e71c}"
    case person = "\u{e60d}"
    case genduo = "\u{e71e}"
    case search = "\u{e71f}"
    case meiyuan = "\u{eb1a}"
    case saomiao = "\u{e721}"
    case yijianfankui = "\u{e722}"
    case tuichu = "\u{e723}"
    case gouwuche = "\u{e724}"
    case liebiao = "\u{e725}"
    case guanyu = "\u{e726}"
    case shouye = "\u{e727}"
    case yisheng = "\u{e601}"
    case weixin = "\u{e620}"
    case pengyouquan = "\u{e60c}"
    case gengduo2 = "\u{e621}"
    case yaodian = "\u{e72a}"
    case shuben = "\u{e600}"
    case weixian = "\u{e71b}"
}
