//
//  JsonUtil.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/30.
//

import UIKit
import HandyJSON

class JsonUtil: NSObject {
    
    /**
     *  Json转对象
     */
    static func jsonToModel<T:HandyJSON>(_ jsonAny:Any?,_ modelType:T.Type) -> T? {
        if let jsonString = jsonAny as? String {
            return T.deserialize(from: jsonString)
        } else if let jsonDic = jsonAny as? Dictionary<String, Any> {
            return T.deserialize(from: jsonDic)
        } else {
            #if DEBUG
            print("TO: \(modelType) ===>  String 为空")
            #endif
            return nil
        }
    }
    
    /**
     *  Json转数组对象
     */
    static func jsonArrayToModel<T:HandyJSON>(_ jsonArrayAny:Any?, _ modelType:T.Type) ->[T]? {
        
        if let json = jsonArrayAny {
            var modelArray:[T] = []
            var peoplesArray = [Any]()
            if let jsonArrayString = json as? String {
                let data = jsonArrayString.data(using: String.Encoding.utf8)
                peoplesArray = try! JSONSerialization.jsonObject(with:data!, options: JSONSerialization.ReadingOptions()) as? [Any]
                    ?? []
                
            } else if let jsonArray = json as? Array<Any> {
                peoplesArray = jsonArray
            }
            
            modelArray = ([T].deserialize(from: peoplesArray) as? [T])!
            return modelArray
            
        } else {
            #if DEBUG
            print("TO: \(modelType) ===> jsonArrayAny为空")
            #endif
            return nil
        }
    }
   
    /**
     *  对象转JSON
     */
    static func modelToJson(_ model:BaseModel?) -> String {
        if model == nil {
            #if DEBUG
            print("modelToJson:model为空")
            #endif
            return ""
        }
        return (model?.toJSONString())!
    }
    
    /**
     *  对象转字典
     */
    static func modelToDictionary(_ model:BaseModel?) -> [String:Any] {
        if model == nil {
            #if DEBUG
            print("modelToJson:model为空")
            #endif
            return [:]
        }
        return (model?.toJSON())!
    }
}

class BaseModel: HandyJSON {
    //    var date: Date?
    //    var decimal: NSDecimalNumber?
    //    var url: URL?
    //    var data: Data?
    //    var color: UIColor?
    
    required init() {}
    
    func mapping(mapper: HelpingMapper) {   //自定义解析规则，日期数字颜色，如果要指定解析格式，子类实现重写此方法即可
        //        mapper <<<
        //            date <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd")
        //
        //        mapper <<<
        //            decimal <-- NSDecimalNumberTransform()
        //
        //        mapper <<<
        //            url <-- URLTransform(shouldEncodeURLString: false)
        //
        //        mapper <<<
        //            data <-- DataTransform()
        //
        //        mapper <<<
        //            color <-- HexColorTransform()
    }
}

