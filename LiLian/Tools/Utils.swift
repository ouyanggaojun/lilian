//
//  Utils.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/30.
//

import UIKit

enum ValidateEnum {
    case email(_:String)
    case phone(_:String)
    case rdCode(_:String)
    
    var isRight:Bool {
        var predicateString:String!
        var currentString:String!
            
        switch self {
        case let .email(str):
            break
          
        case let .phone(str):
            break
            
        case let .rdCode(str):
            predicateString = "^\\d{7,14}|\\d{20}$"
            currentString = str
        default:
            break
        }
        
        let predicate = NSPredicate(format: "SELF MATCHES %@", predicateString)
        return predicate.evaluate(with: currentString)
    }
}
