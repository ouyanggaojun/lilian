//
//  AskDoctorController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import UIKit

class AskDoctorController: BaseWebController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func layoutUI() {
        super.layoutUI()
        wkWebView.snp.remakeConstraints({ (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(kHeight_NaviBarAndStatus)
            make.bottom.equalToSuperview().offset(-kHeight_TabBarAndSafeBottom)
        })
    }
    override func reloadWebView() {
        self.webUrl += "mDoctors.aspx"
        super.reloadWebView()
    }
}
