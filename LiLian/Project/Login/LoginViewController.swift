//
//  LoginViewController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import UIKit


class LoginViewController: BaseWebController {
    
    override func viewDidLoad() {
        webUrl += "/mLogin.aspx"
        super.viewDidLoad()
    }
    
    override func layoutUI() {
        super.layoutUI()
        self.showRightItems = false
    }
}

