//
//  LoginManager.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import Foundation
import HandyJSON
import SwiftyUserDefaults
import WebKit
class LoginManager  {
    static let share = LoginManager()
    
    static var hasLogin:Bool = false
    
    var loginUser:LoginModel = {
        let model = LoginModel()
        return model
    }()
    
    lazy var shareInfo:Dictionary<String, Any> = {
        let dic = [
            "title": kshareTitle,
            "link": releaseUrlWWW,
            "desc": kshareDesc,
            "imgUrl":kshareImg
        ] as [String:Any]
        return dic
    }()
    
    func getLoginInfo() {
        guard let user = Defaults[\.kcookieUser] else {
            didGetCookie(cookieString: nil)
            return
        }
        loginUser = LoginModel.deserialize(from: user) ?? LoginModel()
        LoginManager.hasLogin = true
    }
    
    func didGetCookie(cookieString: String?) {
        
        if let user = decodeValue(value: cookieString ?? nil) {
            Defaults[\.kcookieUser] = user
            loginUser = LoginModel.deserialize(from: user) ?? LoginModel()
            if LoginManager.hasLogin {
                return
            } else {
                LoginManager.hasLogin = true
                NotificationCenter.default.post(name: NSNotification.Name(knotificationName_LoginSuccess), object: nil)
            }
        } else {
            Defaults[\.kcookieUser] = nil
            loginUser = LoginModel()
            LoginManager.hasLogin = false
        }
    }
    
    func decodeValue(value:String?)-> [String: Any]? {
        if var newValue = value {
            newValue = newValue.replacingOccurrences(of: "\'", with: "")
            newValue = newValue.replacingOccurrences(of: "{", with: "")
            newValue = newValue.replacingOccurrences(of: "}", with: "")
            let itemArray = newValue.split(separator: ",")
            var dic:[String:Any] = Dictionary()
            for item in itemArray {
                let subArray = item.split(separator: ":")
                dic[String(subArray[0])] = subArray[1]
            }
            return dic
        }
        return nil
    }
    
    static func loginOut() {
        Defaults[\.kcookieUser] = nil
        Defaults[\.kcookie315] = nil
        LoginManager.share.loginUser = LoginModel()
        LoginManager.hasLogin = false
        LoginManager.clearCookiesForWKWebView()
        NotificationCenter.default.post(name: NSNotification.Name("didLoginOut"), object: nil)
    }
    
    static func clearCookiesForWKWebView() {
        if #available(iOS 11, *) {
            let ofTypes: Set<String> = [WKWebsiteDataTypeCookies,WKWebsiteDataTypeSessionStorage]
            let formater = Date(timeIntervalSince1970: 0)
            WKWebsiteDataStore.default().removeData(ofTypes: ofTypes, modifiedSince: formater) {
            }
        }
    }
}


public struct LoginModel: HandyJSON {
    var ver:Int?
    var id:Int = 0
    var name:String = ""
    var psw:String?
    var admin:Int?
    var admPs:Int?
    var msg:Int?
    var sendOrder:Int?
    var mshopid:Int?
    var mshopOrderQueue:Int?
    var mfactoryid:Int?
    var doctorid:Int?
    var pharmacistID:Int?
    var wx:String = "0"
    var keep:Int?
    var lastSet:Int?
    
    public init() {
        
    }
}
