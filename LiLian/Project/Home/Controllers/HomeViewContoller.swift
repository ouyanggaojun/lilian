//
//  HomeViewContoller.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/25.
//

import UIKit
import HandyJSON
import DropDown
import SwiftyFitsize
import swiftScan
import PPBadgeViewSwift
import SwiftyUserDefaults

class HomeViewContoller: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var headerView : HomeHeaderView = {
        let view = UIView.fromXIB(classType: HomeHeaderView.self)
        view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 235~)
        view.delegate = self
        view.layoutUI()
        return view
    }()
   
    var doctorList:[DoctorModel] = {
        let array = Array<DoctorModel>()
        return array
    }()
    var historyList:[HistoryModel] = {
        let array = Array<HistoryModel>()
        return array
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
        NotificationCenter.default.addObserver(self, selector: #selector(relayoutUI), name: Notification.Name(knotificationName_DidLoginOut), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        relayoutUI()
    }
    
    override func layoutUI() {
        super.layoutUI()
        let view = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 235~))
        view.addSubview(headerView)
        tableView.tableHeaderView = view
        
        tableView.register(UINib(nibName: "HomeViewCell", bundle: Bundle.main), forCellReuseIdentifier: "HomeViewCell")
        tableView.register(UINib(nibName: "HomeViewHistoryCell", bundle: Bundle.main), forCellReuseIdentifier: "HomeViewHistoryCell")
        tableView.register(UINib(nibName: "HomeViewHistoryTwoCell", bundle: Bundle.main), forCellReuseIdentifier: "HomeViewHistoryTwoCell")
        relayoutUI()
    }
    
    @objc func relayoutUI() {
        headerView.hasLogin = LoginManager.hasLogin
        if LoginManager.hasLogin {
            getHistory()
            getNotification()
            
        } else {
            getDoctors()
            self.naviBarItems.rightItems.last?.pp.hiddenBadge()
        }
    }
    
    func getNotification(){
        NetWorkRequest(API.getParams(params: ["cmd" : "getUserHasNewNotification"]), modelType: BaseModel.self) { (resModel, res) in
            if let dic = res as? [String: Any] {
                let count = dic["has"] as? Int ?? 0
                Defaults[\.knotificationCount] = count
                
                if count > 0 {
                    self.naviBarItems.rightItems.first?.pp.addDot(color: .red)
                    self.naviBarItems.rightItems.first?.pp.moveBadge(x: -5, y: 5)
                } else {
                    self.naviBarItems.rightItems.last?.pp.hiddenBadge()
                }
            }
        } failureCallback: { (error) in
            
        }
    }
    
    func getDoctors() {
        NetWorkRequest(API.getParams(params: ["cmd" : "doctors", "num" : 6]), modelType: DoctorModel.self) { (resModel, res) in
            self.doctorList = JsonUtil.jsonArrayToModel(res, DoctorModel.self) ?? []
            self.tableView.reloadData()
        } failureCallback: { (error) in
            
        }
    }
    
    func getHistory() {
        NetWorkRequest(API.getParams(params: ["cmd" : "visitedNews", "mobile" : 1]), modelType: DoctorModel.self) { (resModel, res) in
            self.historyList = JsonUtil.jsonArrayToModel(res, HistoryModel.self) ?? []
            self.tableView.reloadData()
        } failureCallback: { (error) in
            
        }
    }
}

extension HomeViewContoller: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if LoginManager.hasLogin {
            return self.historyList.count
        } else {
            return self.doctorList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90~
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if LoginManager.hasLogin {
            let cell:HomeViewHistoryCell?
            let model = self.historyList[indexPath.row]
            if model.thumb != nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "HomeViewHistoryCell") as? HomeViewHistoryCell
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "HomeViewHistoryTwoCell") as! HomeViewHistoryTwoCell
            }
            
            cell?.selectionStyle = .none
            cell?.model = model
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeViewCell") as! HomeViewCell
            cell.model = self.doctorList[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let VC = DoctorDetailController()
        if LoginManager.hasLogin {
            let url = self.historyList[indexPath.row].url ?? ""
            VC.webUrl = url.contains(releaseUrlWWW) ? url : releaseUrlWWW + "/\(url)"
        } else {
            VC.webUrl = self.doctorList[indexPath.row].url ?? ""
        }
        navigationController?.pushViewController(VC, animated: true)
    }
}

extension HomeViewContoller : HomeHeaderViewDelegate {
    func headerViewDidClick(sender: HomeHeaderViewAction) {
        switch sender {
        case .guanzhuyaodian:
            let VC = PersonCenterController()
            VC.webUrl += "/mMemberShops.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .guanzhuyishen:
            let VC = PersonCenterController()
            VC.webUrl += "/mMemberDoctors.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .allSearch:
            let VC = SearchResultController()
            VC.webUrl += "/search.aspx?where=title&keyword=\(headerView.searchTF.text!)"
            navigationController?.pushViewController(VC, animated: true)
            
        case .allowWenhao:
            let VC = SearchResultController()
            VC.webUrl += "/search.aspx?where=certification&keyword=\(headerView.searchTF.text!)"
            navigationController?.pushViewController(VC, animated: true)
            
        case .code:
            let VC = SearchResultController()
            VC.webUrl += "/search.aspx?where=barcode&keyword=\(headerView.searchTF.text!)"
            navigationController?.pushViewController(VC, animated: true)
            
        case .jibing:
            let VC = SearchResultController()
            VC.webUrl += "/search.aspx?where=disease&keyword=\(headerView.searchTF.text!)"
            navigationController?.pushViewController(VC, animated: true)
            
        case .yaojia:
            let VC = HomeSubItemController()
            VC.webUrl += "mNewMedicine.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .zixun:
            let VC = HomeSubItemController()
            VC.webUrl += "mc66.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .puguang:
            let VC = HomeSubItemController()
            VC.webUrl += "mc14.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .jingli:
            let VC = HomeSubItemController()
            VC.webUrl += "mTreatInfos.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .yaodian:
            let VC = HomeSubItemController()
            VC.webUrl += "mShops.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .yishen:
            let VC = HomeSubItemController()
            VC.webUrl += "mDoctors.aspx"
            navigationController?.pushViewController(VC, animated: true)
            
        case .moreDoc:
            let VC = DoctorListController()
            VC.webUrl += "/mDoctors.aspx"
            navigationController?.pushViewController(VC, animated: true)
        }
    }
}
