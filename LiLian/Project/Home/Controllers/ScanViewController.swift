//
//  ScanViewController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit
import swiftScan
import SnapKit
import SwiftyFitsize

class ScanViewController: LBXScanViewController {
    
    /**
     @brief  扫码区域上方提示文字
     */
    var topTitle: UILabel?
    
    /**
     @brief  闪关灯开启状态
     */
    var isOpenedFlash: Bool = false
    
    // MARK: - 底部几个功能：开启闪光灯、相册、我的二维码
    
    //底部显示的功能项
    var bottomItemsView: UIView?
    
    //相册
    var btnPhoto: UIButton = UIButton()
    
    //闪光灯
    var btnFlash: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //需要识别后的图像
        setNeedCodeImage(needCodeImg: true)
        
        //框向上移动10个像素
        scanStyle?.centerUpOffset += 10
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        drawBottomItems()
    }
    
    override func handleCodeResult(arrayResult: [LBXScanResult]) {
        
        for result: LBXScanResult in arrayResult {
            if let str = result.strScanned {
                print(str)
            }
        }
        let result: LBXScanResult = arrayResult[0] 

        if !ValidateEnum.rdCode(result.strScanned ?? "").isRight {
            MBProgressHUD.ex_showPlainText("扫描结果不是条码，请检查",view: nil)
            drawScanView()
        } else {
        if result.strScanned!.count == 20 {
                NetWorkRequest(API.getToken, modelType: BaseModel.self) { [unowned self] (model, res) in
                    let resJson = res as! [String:String]
                    let token = getNowToken(baseTime: resJson["baseTime"] ?? "", baseToken: resJson["baseToken"] ?? "")
                    
                    let url = "http://www.mashangfangxin.com/drugQueryResult?code=\(String(describing: result.strScanned!))&token=\(token)";
                    let VC = PersonCenterController()
                    VC.webUrl = url
                    self.navigationController?.pushViewController(VC, animated: true)
                } failureCallback: { (error) in
                    
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    let VC = SearchResultController()
                    VC.webUrl += "/search.aspx?where=barcode&keyword=\(result.strScanned!)"
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
        }
    }
   
    func drawBottomItems() {
        if (bottomItemsView != nil) {
            return
        }
        
        bottomItemsView = UIView(frame: CGRect.zero)
        
        self.btnFlash = UIButton()
        btnFlash.setImage("qrcode_scan_btn_flash_nor".image(), for:UIControl.State.normal)
        btnFlash.addTarget(self, action: #selector(ScanViewController.openOrCloseFlash), for: UIControl.Event.touchUpInside)
        
        self.btnPhoto = UIButton()
        btnPhoto.setImage("qrcode_scan_btn_photo_nor".image(), for: UIControl.State.normal)
        btnPhoto.setImage("qrcode_scan_btn_photo_down".image(), for: UIControl.State.highlighted)
        btnPhoto.addTarget(self, action: #selector(openPhotoAlbum), for: UIControl.Event.touchUpInside)
        
        view.addSubview(bottomItemsView!)
        bottomItemsView?.addSubview(btnFlash)
        bottomItemsView?.addSubview(btnPhoto)
        
        bottomItemsView?.snp.remakeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-kHeight_BottomSafeArea)
            make.left.right.equalToSuperview()
            make.height.equalTo(100~)
        }
        self.btnPhoto.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview().offset(-40~)
            make.centerY.equalToSuperview()
            make.width.equalTo(65~)
            make.height.equalTo(87~)
        }
        self.btnFlash.snp.remakeConstraints { (make) in
            make.centerX.equalToSuperview().offset(40~)
            make.centerY.equalToSuperview()
            make.width.equalTo(65~)
            make.height.equalTo(87~)
        }
    }
    
    //开关闪光灯
    @objc func openOrCloseFlash() {
        scanObj?.changeTorch()
        
        isOpenedFlash = !isOpenedFlash
        if isOpenedFlash {
            btnFlash.setImage("qrcode_scan_btn_flash_down".image(), for:UIControl.State.normal)
        } else {
            btnFlash.setImage("qrcode_scan_btn_flash_nor".image(), for:UIControl.State.normal)
        }
    }
    
    
    ///---------------------------------------------
    //10进制转36进制, 1234567890123456 --> c5m8nq6itc
    func C10To36(n10:Int) -> String {
        return Utils.convertDecimalTo36HexStr(UInt64(n10))
    }

    //36进制转10进制, c5m8nq6itc --> 1234567890123456
    func C36To10(s36:String)->Int {
        return Int(Utils.convert36HexStr(toDecimalWith36HexStr: s36));
    }

    //示例var baseTime = "2020-02-22 11:51:00";
    //var baseToken = "2FVH65CU";
    func getNowToken(baseTime:String, baseToken:String) -> String {
        let d1 =  baseTime.stringConvertDate().second
        let d2 =  Date().second
        var ds = (d2 - d1) / 1000  //相差多少秒

        var kday = 0;
        var khour = 0;
        khour = Int(ds / 3600);
        kday = Int(ds / 3600 / 24);
        ds = ds + UInt(kday * 456000);
        ds = ds + UInt(khour * 2400);
        let vt1 = C36To10(s36: baseToken);
        var vt2 = Double(vt1) + Double(ds) * 100.0 / 60;
        vt2 = Double(Int(vt2)); //去掉小数
        let ret = C10To36(n10: Int(vt2)).uppercased(); //转为大写
        return ret;
    }

}

