//
//  AboutAppController.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit

class AboutAppController: BaseViewController {

    @IBOutlet weak var newVersion: UILabel!
    @IBOutlet weak var currentVersion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "关于我们"
        layoutUI()
    }
    
    override func layoutUI() {
        let iOSVersion = UIDevice.appVersion()
        self.currentVersion.text = "当前版本: \(iOSVersion)"
        
        NetWorkRequest(API.getAppVersion, modelType: DoctorModel.self) { (resModel, res) in
            let json = res as? [String:Any]
            self.newVersion.text = "最新版本: \(json?["ios"] ?? iOSVersion)"
        } failureCallback: { (error) in
            
        }
    }
    
    @IBAction func clearCach(_ sender: UIButton) {
        UIDevice.removeCache()
    }
}
