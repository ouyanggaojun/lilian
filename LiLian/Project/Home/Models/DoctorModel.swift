//
//  DoctorModel.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit

class DoctorModel: BaseModel {
    var department: String?
    var hospital: String?
    var isOnline: Bool = false
    var name: String?
    var photo: String?
    var professionalTitle: String?
    var skilledIn: String?
    var url: String?
    var verified: Bool = false
    
    required init() {
        
    }
}





class HistoryModel: BaseModel {
    var date: String?
    var remark: String?
    var summary: String?
    var title: String?
    var url: String?
    
    var certification:String?
    var factory: String?
    var instock: Bool = false
    var model: String?
    var thumb: String?
    
    
    required init() {
        
    }
}
