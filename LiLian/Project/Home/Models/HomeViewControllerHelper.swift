//
//  HeaderRightItemsView.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/31.
//

import UIKit
import WebKit


protocol HomeViewControllerHelperDelegate {
    func rightItemsDidClick(sender:UIButton)
}

class HomeViewControllerHelper: NSObject {
    
    var delegate:HomeViewControllerHelperDelegate?
    lazy var rightItems:[UIBarButtonItem] = {
        var items = Array<UIBarButtonItem>()
        let titles = [IconFont.genduo,IconFont.fenxiang,IconFont.saomiao, IconFont.search]
        for title in titles {
            let index = titles.firstIndex(of: title) ?? 0
            let buttonItem = UIButton(type: .custom)
            buttonItem.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
            buttonItem.tag = 10 + index
            buttonItem.setImage(UIImage(iconfont: title, iconColor: MainColor, size: CGSize(width: 25, height: 25)), for: .normal)
            buttonItem.setImage(UIImage(iconfont: title, iconColor: MainColor, size: CGSize(width: 25, height: 25)), for: .selected)
            buttonItem.addTarget(self, action: #selector(rightItemsClick(sender:)), for: .touchUpInside)
            let item = UIBarButtonItem(customView: buttonItem)
            items.append(item)
        }
        return items;
    }()
    
    var webUrl:String = "https://www.315jiage.cn/"
    
    lazy var wkWebView:WKWebView = {
        //配置环境
        let configuration = WKWebViewConfiguration()
        let userContentController = WKUserContentController()
        configuration.userContentController = userContentController;
        let webView = WKWebView(frame: CGRect.zero, configuration: configuration)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.clipsToBounds = true
        return webView
    }()
    
    @objc func rightItemsClick(sender: UIButton) {
        delegate?.rightItemsDidClick(sender: sender)
    }
    
    func reloadWebView() {
        self.wkWebView.load(URLRequest(url: URL(string: webUrl)!))
    }
}

extension HomeViewControllerHelper : WKNavigationDelegate, WKUIDelegate {
    
}
