//
//  HomeViewHistoryCell.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit

class HomeViewHistoryCell: UITableViewCell {
    @IBOutlet weak var nameLab: UILabel!
    
    @IBOutlet weak var desLab1: UILabel!
    @IBOutlet weak var desLab2: UILabel!
    @IBOutlet weak var desLab3: UILabel!
    @IBOutlet weak var thumbImageV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    var model: HistoryModel? {
        didSet{
            nameLab.text = model?.title
            if let fatory = model?.factory {
                //药品
                let url = URL(string: releaseUrlWWW + "/" + (model?.thumb ?? ""))
                thumbImageV.kf.setImage(with: url)
                desLab1.text = "规格: \(model?.model ?? "")"
                desLab2.text = "批准文号: \(model?.certification ?? "")"
                desLab3.text = "生产厂家: \(fatory)"
            } else {
                //药店
                desLab1.text = "日期: \(model?.date ?? "")"
                desLab2.text = "简介: \(model?.summary ?? "")"
                desLab3.text = "评论: \(model?.remark ?? "")"
            }
        }
    }
}
