//
//  HomeHeaderView.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/31.
//

import UIKit

enum HomeHeaderViewAction {
    case allSearch
    case allowWenhao
    case code
    case jibing
    case yaojia
    case zixun
    case puguang
    case jingli
    case yaodian
    case yishen
    case guanzhuyaodian
    case guanzhuyishen
    case moreDoc
}

protocol HomeHeaderViewDelegate {
    func headerViewDidClick(sender: HomeHeaderViewAction)
}


class HomeHeaderView: UIView {
    var delegate:HomeHeaderViewDelegate?
    
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var searchSenderButton: UIButton!
    @IBOutlet weak var searchTF: UITextField!

    @IBOutlet weak var unLoginHearder: UIView!
    @IBOutlet weak var loginHeader: UIView!
    
    var hasLogin:Bool=false {
        didSet {
            if hasLogin {
                self.loginHeader.isHidden = false
                self.unLoginHearder.isHidden = true
            } else {
                self.loginHeader.isHidden = true
                self.unLoginHearder.isHidden = false
            }
        }
    }

    func layoutUI() {
        searchSenderButton.addCornerBezier(conrners: [.topRight, .bottomRight], radius: 4)
    }
    
    @IBAction func myAttentionShop(_ sender: UIButton) {
        delegate?.headerViewDidClick(sender: .guanzhuyaodian)
    }
    
    @IBAction func myAttentionDoctor(_ sender: UIButton) {
        delegate?.headerViewDidClick(sender: .guanzhuyishen)
    }
    
    @IBAction func searchType(_ sender: UIButton) {
        if searchButton != nil {
            searchButton?.isSelected = false
        }
        sender.isSelected = true;
        searchButton = sender
        
        switch sender.tag {
        case 10:
            searchTF.placeholder = "请填写搜索关键字"
            break
        case 11:
            searchTF.placeholder = "请输入批准文号中的数字"
            break
        case 12:
            searchTF.placeholder = "请输入全数字条码"
            break
        case 13:
            searchTF.placeholder = "请输入疾病名称搜药品"
            break
        default:
            break;
        }
    }
    
    
    @IBAction func searchClick(_ sender: UIButton) {
        let searchText = searchTF.text
        if searchText!.count > 0 {
            switch searchButton.tag {
            case 10:
                delegate?.headerViewDidClick(sender: .allSearch)
                break
            case 11:
                delegate?.headerViewDidClick(sender: .allowWenhao)
                break
            case 12:
                delegate?.headerViewDidClick(sender: .code)
                break
            case 13:
                delegate?.headerViewDidClick(sender: .jibing)
                break
            default:
                break;
            }
        } else {
            MBProgressHUD.ex_showPlainText("请填写搜索关键字",view: nil)
        }
    }
    @IBAction func subItemClick(_ sender: UIButton) {
        switch sender.tag {
        case 10:
            delegate?.headerViewDidClick(sender: .yaojia)
            break
        case 11:
            delegate?.headerViewDidClick(sender: .zixun)
            break
        case 12:
            delegate?.headerViewDidClick(sender: .puguang)
            break
        case 13:
            delegate?.headerViewDidClick(sender: .jingli)
            break
        case 14:
            delegate?.headerViewDidClick(sender: .yaodian)
            break
        case 15:
            delegate?.headerViewDidClick(sender: .yishen)
            break
        default:
            break;
        }
    }
    
    @IBAction func moreDoctor(_ sender: UIButton) {
        delegate?.headerViewDidClick(sender: .moreDoc)
    }
    
}
