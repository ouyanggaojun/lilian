//
//  HomeDropDownCell.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/27.
//

import UIKit
import DropDown

class HomeDropDownCell: DropDownCell {

    @IBOutlet weak var iconImageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
