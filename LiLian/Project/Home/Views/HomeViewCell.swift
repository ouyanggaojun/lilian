//
//  HomeViewCell.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/26.
//

import UIKit
import Kingfisher
import Hue
import SwiftyFitsize

class HomeViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var hospitalLb: UILabel!
    @IBOutlet weak var skilledInLb: UILabel!
    
    @IBOutlet weak var headImageView: UIImageView!
    
    lazy var professionalTitlebutton:EWLayoutButton = {
        let button = EWLayoutButton(type: .custom)
        button.setTitle("", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.init(rgbHexString: "#5bc0de")
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        button.midSpacing = 0;
        button.titleLabel?.font = .systemFont(ofSize: 12~)
        button.addCornerLayer(radius: 2)
        return button
    }()
    
    lazy var verifiedbutton:EWLayoutButton = {
        let button = EWLayoutButton(type: .custom)
        button.setTitle("已验证", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.init(rgbHexString: "#f0ad4e")
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        button.midSpacing = 0;
        button.titleLabel?.font = .systemFont(ofSize: 12~)
        button.addCornerLayer(radius: 2)
        return button
    }()
    
    lazy var isOnlinebutton:EWLayoutButton = {
        let button = EWLayoutButton(type: .custom)
        button.setTitle("在线", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor.init(rgbHexString: "#d9534f")
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        button.midSpacing = 0;
        button.titleLabel?.font = .systemFont(ofSize: 12~)
        button.addCornerLayer(radius: 2)
        return button
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layoutUI()
    }
    
    func layoutUI() {
        self.contentView.addSubview(professionalTitlebutton)
        self.contentView.addSubview(verifiedbutton)
        self.contentView.addSubview(isOnlinebutton)
        
        professionalTitlebutton.snp.remakeConstraints { (make) in
            make.centerY.equalTo(nameLb)
            make.left.equalTo(nameLb.snp.right).offset(3)
            make.height.equalTo(17~)
        }
        verifiedbutton.snp.remakeConstraints { (make) in
            make.centerY.equalTo(nameLb)
            make.left.equalTo(professionalTitlebutton.snp.right).offset(3)
            make.height.equalTo(17~)
            make.width.equalTo(48~)
        }
        isOnlinebutton.snp.remakeConstraints { (make) in
            make.centerY.equalTo(nameLb)
            make.left.equalTo(verifiedbutton.snp.right).offset(3)
            make.height.equalTo(17~)
            make.width.equalTo(36~)
        }
        headImageView.addCornerLayer(radius: 30~)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var model: DoctorModel? {
        didSet{
            let url = URL(string: model?.photo ?? "")
            headImageView.kf.setImage(with: url)
            nameLb.text = model?.name
            hospitalLb.text = model?.hospital
            skilledInLb.text = model?.skilledIn
            professionalTitlebutton.setTitle(model?.professionalTitle, for: .normal)
            
            
            // 更新UI
            verifiedbutton.snp.updateConstraints { (make) in
                make.width.equalTo(model?.verified ?? false ? 45~ : 0)
            }
            
            isOnlinebutton.snp.updateConstraints { (make) in
                make.left.equalTo(verifiedbutton.snp.right).offset(model?.verified ?? false ? 3 : 0)
                make.width.equalTo(model?.isOnline ?? false ? 36~ : 0)
            }
        }
    }
}
