//  LiLian
//
//  Created by ouyanggaojun on 2020/12/23.
//
import Foundation
import Moya
import SwiftyUserDefaults

enum API {
    case getLoginInfo
    case getParams(params:[String: Any])
    case getAppVersion
    case getToken
    case wxBind(params:[String: Any])
    case postParams(params:[String: Any])
}

extension API:TargetType {
    var baseURL: URL {
        switch self {
        case .getToken:
            return URL.init(string: isRelease ? releaseUrlApp : debugUrl)!
            
        case .getAppVersion:
            return URL.init(string: isRelease ? releaseUrlApp : debugUrl)!
        default:
            return URL.init(string: isRelease ? releaseUrlWWW : debugUrl)!
        }
    }
    
    var path: String {
        switch self {
        case .getLoginInfo:
            return "/appJsonUser.aspx"
        case .getParams:
            return "/ajax.aspx"
        case .postParams:
            return "/ajax.aspx"
        case .getAppVersion:
            return "/apk/versionnumber.json"
        case .getToken:
            return "/apk/token.json"
        case .wxBind:
            return "/mWxLogin.aspx"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .postParams:
            return .post
        default:
            return .get
        }
    }

    //这个是做单元测试模拟的数据，必须要实现，只在单元测试文件中有作用
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }

    //该条请API求的方式,把参数之类的传进来
    var task: Task {
        // return .requestParameters(parameters: nil, encoding: JSONArrayEncoding.default)
        switch self {
        case .getLoginInfo:
            return .requestPlain
           
        case let .getParams(params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case let .postParams(params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case let .wxBind(params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case .getAppVersion:
            return .requestPlain
            
        case .getToken:
            return .requestPlain
        }
    }
     
    var headers: [String : String]? {
        switch self {
        case .postParams:
            return ["Content-Type": "application/x-www-form-urlencoded"]
            
        default:
            return setWebCookie(header: ["Content-Type" : "application/json;charset=utf-8"]) 
        }
    }
    
    
    // 添加cookie
    func setWebCookie(header:[String:String]?)->[String:String] {
        var newHeader = header ?? Dictionary<String,String>()
        guard let cookieDictionary = Defaults[\.kcookie315] else {
            return newHeader
        }
        var cookieString = ""
        for cookie in cookieDictionary {
            if cookie.key == "iwmsUser" {
                if let user = Defaults[\.kcookieUser] {
                    cookieString.append(cookie.key + "={")
                    for item in user {
                        if item.value is String {
                            cookieString.append("""
                                 "\(item.key)":\'\(item.value)\',
                                """)
                        } else {
                            cookieString.append("""
                                 "\(item.key)":\(item.value),
                                """)
                        }
                    }
                    cookieString.append("};")
                }
            } else {
                cookieString.append(cookie.key + "=" + cookie.value + ";")
            }
        }
        newHeader["Cookie"] = cookieString
        return newHeader
    }

}
