//
//  MoyaPlugin.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/23.
//

import Foundation
import Moya



/// Moya插件: 网络请求时显示loading...
internal final class LoadingPlugin: PluginType {
    
    /// 在发送之前调用来修改请求
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var request = request
        request.timeoutInterval = 15 //超时时间
        return request
    }
    
    /// 在通过网络发送请求(或存根)之前立即调用
    func willSend(_ request: RequestType, target: TargetType) {
        guard let target = target as? APIDemo
            else { return }
        /// 判断是否需要显示
//        target.showStats ? SVProgressHUD.show() : ()
//        SVProgressHUD.setDefaultMaskType(.clear)
    }
    
    /// 在收到响应之后调用，但是在MoyaProvider调用它的完成处理程序之前调用
    func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {
        /// 0.2s后消失
        MBProgressHUD.ex_hide()
    }
    
    /// 调用以在完成之前修改结果
//    func process(_ result: Result<Response, MoyaError>, target: TargetType) -> Result<Response, MoyaError> {}
}


/// Moya插件: 控制台打印请求的参数和服务器返回的json数据
internal final class PrintParameterAndJsonPlugin: PluginType {
    
    func willSend(_ request: RequestType, target: TargetType) {
        #if DEBUG
        // 打印请求参数
        if let requestData = request.request?.httpBody {
            print("发送\(request.request?.httpMethod ?? "")请求 ==> "
                    + "\(request.request?.url?.absoluteString ?? "")" + "\n"
                    + "参数: "
                    + "\(String(data: requestData, encoding: String.Encoding.utf8) ?? "")"
                )
        } else {
            print("发送\(request.request?.httpMethod ?? "")请求 ==> "
                    + "\(String(describing: request.request?.url!))"
                    )
        }
        #endif
    }
    
    func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {
        #if DEBUG
        switch result {
        case .success(let response):
            do {
                let jsonObiect = try response.mapJSON()
                print("""
                    请求成功 ==> \(String(describing: response.request?.url?.absoluteString))
                    \(jsonObiect)
                    """)
            } catch {
                let dataString = String(data:response.data ,encoding: String.Encoding.utf8)
                print("""
                    请求成功 ==> \(String(describing: response.request?.url?.absoluteString))
                    \(dataString)
                    """)
            }
            break
        default:
            print("""
                请求异常 ==> \(target.path)
            """)
            break
        }
        #endif
    }
}
