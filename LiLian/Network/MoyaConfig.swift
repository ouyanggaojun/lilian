//  LiLian
//
//  Created by ouyanggaojun on 2020/12/23.
//

import Foundation


/// 是否是发布版本
public let isRelease: Bool = true

/// 发布域名
public let releaseUrlWWW = "https://www.315jiage.cn"

public let releaseUrlApp = "https://app.315jiage.cn"


/// 测试域名
public let debugUrl = "https://www.315jiage.cn"


import HandyJSON

/// 网络请求返回数据结构
public struct NetworkResponse: HandyJSON {

    var code: Int = 0
    var message: String?
    var result: Any?
    
    public init() {
        self.init(code: 0, message: nil, result: nil )
    }
    
    public init(code: Int, message: String?, result: Any?) {
        self.code = code
        self.message = message
        self.result = result
    }
}

/// 各code代表什么
public enum HttpStatus: Int {
    case success = 200 // 成功
    case logout = 208 // 登出
    case requestFailed = 300 //网络请求失败
    case noDataOrDataParsingFailed = 301 //无数据或解析失败
}

public enum HttpError: Swift.Error {
    case requestFailed(error: Error?) //网络请求失败
    case noDataOrDataParsingFailed(error: Error?) //无返回数据或数据解析失败
    case operationFailure(resultCode: Int?, resultMsg: String?) //操作失败
    case logout //登出
    case failed(error: Error?) // 失败
    case failedNormal(error: String?) //普通失败
}

// MARK: - 输出error详细信息
extension HttpError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .requestFailed(let error):
            return "====>>> 网络请求失败: \(String(describing: error))"
        case .noDataOrDataParsingFailed(let error):
            return "====>>> 无返回数据或数据解析失败: \(String(describing: error)) "
        case .operationFailure(let resultCode, let resultMsg):
            guard let resultCode = resultCode,
                let resultMsg = resultMsg else {
                    return "====>>> 操作失败 "
            }
            return "====>>> 错误码: " + String(describing: resultCode) + ", 错误信息: " + resultMsg
        case .logout:
            // FIXME: - =======进行登出操作======
            return "====>>> 登录过期,需登出"
        case .failed(let error):
            return "====>>> 失败: \(String(describing: error))"
        case .failedNormal(let error):
            return "====>>> \(String(describing: error))"
        }
    }
}

