//  LiLian
//
//  Created by ouyanggaojun on 2020/12/23.
//

import Alamofire
import Foundation
import Moya
import HandyJSON
import SwiftyUserDefaults


/// 超时时长
private var requestTimeOut: Double = 30

// 成功回调
typealias RequestSuccessCallback = ((_ model: NetworkResponse?, _ resposneData: Any) -> Void)

// 失败回调
typealias RequestFailureCallback = ((_ error: HttpError) -> Void)



/// 网络请求的基本设置,这里可以拿到是具体的哪个网络请求，可以在这里做一些设置
private let myEndpointClosure = { (target: TargetType) -> Endpoint in
    /// 这里把endpoint重新构造一遍主要为了解决网络请求地址里面含有? 时无法解析的bug https://github.com/Moya/Moya/issues/1198
    let url = target.baseURL.absoluteString + target.path
    var task = target.task
    
    /*
     如果需要在每个请求中都添加类似token参数的参数请取消注释下面代码
     👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇👇
     */
    //    let additionalParameters = ["token":"888888"]
    //    let defaultEncoding = URLEncoding.default
    //
    //    switch target.task {
    //        ///在你需要添加的请求方式中做修改就行，不用的case 可以删掉。。
    //    case .requestPlain:
    //        task = .requestParameters(parameters: additionalParameters, encoding: defaultEncoding)
    //
    //    case .requestParameters(var parameters, let encoding):
    //        additionalParameters.forEach { parameters[$0.key] = $0.value }
    //        task = .requestParameters(parameters: parameters, encoding: encoding)
    //
    //    default:
    //        break
    //    }
    
    var endpoint = Endpoint(
        url: url,
        sampleResponseClosure: { .networkResponse(200, target.sampleData) },
        method: target.method,
        task: task,
//        httpHeaderFields:target.headers
        httpHeaderFields:target.headers
    )
    
    // 每次请求都会调用endpointClosure 到这里设置超时时长 也可单独每个接口设置
    requestTimeOut = 30
    
    // 针对于某个具体的业务模块来做接口配置
    if let apiTarget = target as? API {
        switch apiTarget {

        default:
            return endpoint
        }
    }
    return endpoint
}

/// 网络请求的设置
private let requestClosure = { (endpoint: Endpoint, done: MoyaProvider.RequestResultClosure) in
    do {
        var request = try endpoint.urlRequest()
        // 设置请求时长
        request.timeoutInterval = requestTimeOut
        done(.success(request))
    } catch {
        done(.failure(MoyaError.underlying(error, nil)))
    }
}

/*   设置ssl
 let policies: [String: ServerTrustPolicy] = [
 "example.com": .pinPublicKeys(
 publicKeys: ServerTrustPolicy.publicKeysInBundle(),
 validateCertificateChain: true,
 validateHost: true
 )
 ]
 */

// 用Moya默认的Manager还是Alamofire的Manager看实际需求。HTTPS就要手动实现Manager了
// private public func defaultAlamofireManager() -> Manager {
//
//    let configuration = URLSessionConfiguration.default
//
//    configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
//
//    let policies: [String: ServerTrustPolicy] = [
//        "ap.grtstar.cn": .disableEvaluation
//    ]
//    let manager = Alamofire.SessionManager(configuration: configuration,serverTrustPolicyManager: ServerTrustPolicyManager(policies: policies))
//
//    manager.startRequestsImmediately = false
//
//    return manager
// }

// https://github.com/Moya/Moya/blob/master/docs/Providers.md  参数使用说明
// stubClosure   用来延时发送网络请求

/// 网络请求发送的核心初始化方法，创建网络请求对象
let Provider = MoyaProvider<MultiTarget>(endpointClosure: myEndpointClosure, requestClosure: requestClosure, plugins: [LoadingPlugin(), PrintParameterAndJsonPlugin()], trackInflights: false)

/// 带有模型转化的底层网络请求的基础方法    可与 179 行核心网络请求方法项目替换 唯一不同点是把数据转模型封装到了网络请求基类中
///  本方法只写了大概数据转模型的实现，具体逻辑根据业务实现。
/// - Parameters:
///   - target: 网络请求接口
///   - successCallback: 网络请求成功的回调 转好的模型返回出来
///   - failureCallback: 网络请求失败的回调
/// - Returns: 可取消网络请求的实例
@discardableResult
func NetWorkRequest<T: HandyJSON>(_ target: TargetType, showLoading: Bool = false, modelType: T.Type?, successCallback:@escaping  RequestSuccessCallback, failureCallback:@escaping RequestFailureCallback) -> Cancellable? {
    
    // 先判断网络是否有链接 没有的话直接返回--代码略
    if !UIDevice.isNetworkConnect {
        print("网络异常=========")
        MBProgressHUD.ex_showPlainText("网络异常", view: nil)
        failureCallback(HttpError.failedNormal(error: "网络异常"))
        return nil
    }
    
    // 这里显示loading图
    if showLoading {
        MBProgressHUD.ex_showActivityLoading()
    }
    
    return Provider.request(MultiTarget(target)) { result in
        // 隐藏hud
        if showLoading {
            MBProgressHUD.ex_hide(for: nil)
        }
        
        switch result {
        case let .success(response):
            do {
                var jsonData: AnyObject?
                    
                jsonData = try JSONSerialization.jsonObject(with: response.data as Data, options: .mutableContainers) as AnyObject
                
                if jsonData == nil  {
                    jsonData = String(data:response.data ,encoding: String.Encoding.utf8)?.toDictionary() as AnyObject
                }
                
//                if jsonData.dictionaryObject != nil {
//                    // 字典转model
//                    if var model = NetworkResponse.deserialize(from: jsonData.dictionaryObject) {
//                        if model.result is [String:Any] {
//                            // result字典  转 model
//                            if let resultModel = T.deserialize(from: model.result as? [String:Any]) {
//                                model.result = resultModel
//                            }
//                        } else if model.result is [[String: Any]] {
//                            // result数组 转 models
//                            if let resultModels = [T].deserialize(from: model.result as? [[String: Any]]) as? [T] {
//                                model.result = resultModels
//                            }
//                        }
//                        successCallback(model, jsonData)
//                    } else {
//                        successCallback(nil, jsonData)
//                    }
//                } else {
                    successCallback(nil, jsonData)
//                }
            } catch {
                
            }
        case let .failure(error):
            // 网络连接失败，提示用户
            print("网络连接失败\(error)")
            failureCallback(HttpError.failedNormal(error: error.localizedDescription))
        }
    }
}

/// 基于Alamofire,网络是否连接，，这个方法不建议放到这个类中,可以放在全局的工具类中判断网络链接情况
/// 用计算型属性是因为这样才会在获取isNetworkConnect时实时判断网络链接请求，如有更好的方法可以fork
extension UIDevice {
    static var isNetworkConnect: Bool {
        let network = NetworkReachabilityManager()
        return network?.isReachable ?? true // 无返回就默认网络已连接
    }
}
