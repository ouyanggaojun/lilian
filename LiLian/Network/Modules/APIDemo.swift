//
//  DemoApi.swift
//  LiLian
//
//  Created by ouyanggaojun on 2020/12/23.
//

import Foundation

import Moya

// 注册登录模块的api, 仅仅做多业务拆分的演示。 具体网络请求接口封装可以参照API.swift文件
enum APIDemo {
    
    case updateAPi(parameters:[String:Any])
    
    case register(email:String,password:String)
    
    case uploadHeadImage(parameters: [String:Any], imageDate:Data)
    
    case getRequset(parameters:[String:Any])
    
    case posRequest(parameters:[String: Any])
}

extension APIDemo : TargetType {
    var baseURL: URL {
        switch self {
        case .register:
            return URL.init(string:"http://news-at.zhihu.com/api/")!
            
        default:
            return URL.init(string: isRelease ? releaseUrlWWW : debugUrl)!
        }
    }
    
    var path: String {
        switch self {
        case .getRequset:
            return "/poetryFull"
            
        case .posRequest:
            return "/poetryFull"
            
        case .updateAPi:
            return "versionService.getAppUpdateApi"
            
        case .uploadHeadImage:
            return "/file/user/upload.jhtml"
        default:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getRequset:
            return .get
        default:
            return .post
        }
    }
    
    // 这个是做单元测试模拟的数据，必须要实现，只在单元测试文件中有作用
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    // 该条请API求的方式,把参数之类的传进来
    var task: Task {
        // return .requestParameters(parameters: nil, encoding: JSONArrayEncoding.default)
        switch self {
        
        case let .register(email, password):
            return .requestParameters(parameters: ["email": email, "password": password], encoding: JSONEncoding.default)
            
        case let .getRequset(paramete):
            return .requestParameters(parameters: paramete, encoding: URLEncoding.default)
            
        case let .posRequest(parameters):
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
            
        case let .updateAPi(parameters):
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            
        //图片上传
        case .uploadHeadImage(let parameters, let imageDate):
            ///name 和fileName 看后台怎么说，   mineType根据文件类型上百度查对应的mineType
            let formData = MultipartFormData(provider: .data(imageDate), name: "file",
                                             fileName: "hangge.png", mimeType: "image/png")
            return .uploadCompositeMultipart([formData], urlParameters: parameters)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type":"application/x-www-form-urlencoded"]
    }
    
}

